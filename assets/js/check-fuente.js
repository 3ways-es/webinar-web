/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    $.ajax({
        type: 'POST',
        url: '/api/users_tiempos',
        dataType: 'json',
        success: function(data) {
            if (!data.success)
                return;
            
            changeIframeSource('unregister-time', ('/storage/unregister.html?payload=' + data.payload));
        }
    });

    if (($('#video-area').data('id') == 2) && isIE()) {
        $('#change_source-modal').modal('show');
        $('#change_source-modal').on('shown.bs.modal', function() {
            requestFuente(1);
        });
    }
});

let sourceInterval = setInterval(checkSource, 15000);
window.onunload = unregisterTime;

function unregisterTime() {
    changeIframeSource('unregister-time', '');
    return null;
}

function changeIframeSource(id, source) {
    const iframe = document.getElementById(id);
    if (!iframe)
        return;

    iframe.src = source;
}

function checkSource() {
    $.ajax('/storage/fuentes.html?' + Date.now(), {
        success: function (data) {
            data = JSON.parse(data);
            let disableInterval = data.find(function(source) {
                return (source.idControlFuente == 6 && source.activo == 1);
            });

            if (disableInterval) {
                clearInterval(sourceInterval);
                sourceInterval = null;

                return;
            }
            
            let payload = data.find(function(source) {
                return (source.idControlFuente != 6 && source.activo == 1);
            });

            if (payload.tipo == 'vimeo' && isIE())
                return;

            changeSource(payload);
            changeChatStatus(payload);
        }
    })
}

function changeSource(payload) {
    if (!payload)
        return;
    
    if (payload.idControlFuente != document.getElementById('video-area').dataset.id) {
        document.getElementById('video-area').dataset.id = payload.idControlFuente;

        if (payload.tipo == 'image') {
            $('#video').attr('src', '');
            
            let content = payload.source;
            if(payload.audio)
                content += '<audio autoplay><source src="' + payload.audio + '" type="audio/mpeg"></audio>';
                
            $('#video').addClass('d-none');
            $('.content-imagen').removeClass('d-none');
            $('#source-image').attr('src', payload.source);
        } else {
            $('#video').removeClass('d-none');
            $('.content-imagen').addClass('d-none');
            $('#source-image').attr('src', '');
            
            let content = payload.source;
            if(payload.audio)
                content += '<audio autoplay><source src="' + payload.audio + '" type="audio/mpeg"></audio>';
            
            $('#video').attr('src', payload.source);
        }
    }
}

function changeChatStatus(payload) {
    if (!payload)
        return;

    const isChatEnabled = (((payload.chat && payload.chat == 1) && payload.chat_source) || false);
    if (isChatEnabled != $('#chat-area').data('enabled')) {
        $('#chat-area').data('enabled', isChatEnabled);
        $('#video-area').attr('class', ('source-area ' + (isChatEnabled ? 'col-12 col-lg-8' : 'col-12 no-chat')));
        
        if (isChatEnabled) {
            $('#chat').attr('src', payload.chat_source);
            $('#chat-area').removeClass('d-none');
        } else {
            $('#chat').attr('src', '');
            $('#chat-area').addClass('d-none');
        }
    }
}

function requestFuente(id) {
    if (!id)
        return;
    
    $.ajax({
        url: '/api/control_fuentes/' + id,
        dataType: 'json',
        success: function (data) {
            if (!data.success)
                return;
            
            changeSource(data.payload);
            changeChatStatus(data.payload);
        }
    });
}