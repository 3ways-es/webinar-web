/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

window.onload = function() {
    let submitting = false;

    // Recover password action
    if (document.getElementById('recover-form'))
        document.getElementById('recover-form').addEventListener('submit', function(e) {
            e.preventDefault();

            if (submitting)
                return;

            const email = document.getElementById('input-email');
            const recaptcha = document.getElementById('input-recaptcha');

            const emailFeedback = email.nextElementSibling;
            const recaptchaFeedback = recaptcha.nextElementSibling;

            if (!email.value || !grecaptcha.getResponse()) {
                if (!email.value) {
                    email.classList.add('is-invalid');
                    emailFeedback.innerText = 'El campo de email no puede estar vacio';
                } else {
                    email.classList.remove('is-invalid');
                    emailFeedback.innerText = '';
                }

                if (!grecaptcha.getResponse()) {
                    recaptcha.classList.add('is-invalid');
                    recaptchaFeedback.innerText = 'Debe realizar la comprobación del captcha';
                } else {
                    recaptcha.classList.remove('is-invalid');
                    recaptchaFeedback.innerText = '';
                }

                return;
            }

            submitting = true;
            $.ajax({
                type: 'POST',
                url: '/api/users/recover',
                data: {
                    email: email.value,
                    'g-recaptcha-response': grecaptcha.getResponse()
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        window.location.href = data.sendTo;
                        return;
                    }

                    if (data.errors.email) {
                        email.classList.add('is-invalid');
                        data.errors.email.forEach(function(error) {
                            emailFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        email.classList.remove('is-invalid');
                        emailFeedback.innerText = '';
                    }

                    if (data.errors.recaptcha) {
                        recaptcha.classList.add('is-invalid');
                        data.errors.recaptcha.forEach(function(error) {
                            recaptchaFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        recaptcha.classList.remove('is-invalid');
                        recaptchaFeedback.innerText = '';
                    }
                    
                    submitting = false;
                },
                complete: function() {
                    submitting = false;
                }
            });
        });
};
