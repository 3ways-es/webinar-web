/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    const cookieBox = document.getElementById('cookie-box');
    if (cookieBox) {
        document.getElementById('close-cookies').addEventListener('click', function() {
            createCookie('cookie-banner', '1');
            cookieBox.style.display = 'none';
        });

        $.ajax({
            type: 'get',
            url: '/api/cookies/cookie-banner',
            dataType: 'json',
            success: function(data) {
                if (!data.exists || data.value != 1)
                    cookieBox.style.display = 'block';
            }
        });   
    }
    
    const disabledLinks = document.querySelectorAll('a.disabled');
    for (let i = 0; i < disabledLinks.length; i++) {
        const disabledLink = disabledLinks[i];
        const imageElement = disabledLink.querySelector('img');
        
        if (imageElement) {
            imageElement.src = grayScale(imageElement);
            imageElement.classList.remove('button');
        }
        
        disabledLink.href = '';
        disabledLink.addEventListener('click', function(e) {
            e.preventDefault();
        });
    }
});

function createCookie(cname, cvalue) {
    $.ajax({
        type: 'post',
        url: '/api/cookies',
        data: {
            cname: cname,
            cvalue: cvalue
        },
        dataType: 'json',
        success: function(data) {
            if (!data.success)
                console.log('Error al guardar cookie');
            else
                console.log('Cookie guardada correctamente: ' + cname);
        }
    });
}

function isIE() {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE ');
    
    return ((msie > 0) || !!navigator.userAgent.match(/Trident.*rv\:11\./));
}

function grayScale(imgObj) {
    const canvas = document.createElement('canvas');
    const canvasContext = canvas.getContext('2d');
    const imgW = imgObj.width;
    const imgH = imgObj.height;

    canvas.width = imgW;
    canvas.height = imgH;
    canvasContext.drawImage(imgObj, 0, 0);
    
    const imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
    for(let y = 0; y < imgPixels.height; y++)
        for(let x = 0; x < imgPixels.width; x++) {
            const i = ((y * 4) * imgPixels.width + x * 4);
            const avg = ((imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3);

            imgPixels.data[i] = avg; 
            imgPixels.data[i + 1] = avg; 
            imgPixels.data[i + 2] = avg;
        }

    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
}

// Polyfills
//-----------------------------------
// includes
if (!String.prototype.includes) {
    String.prototype.includes = function() {
        'use strict';

        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

// find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function(predicate) {
            if (this == null)
                throw new TypeError('"this" is null or not defined');
  
            const o = Object(this);
            const length = (o.length >>> 0);

            if (typeof predicate !== 'function')
                throw new TypeError('predicate must be a function');
            
            const arg = arguments[1];
            let k = 0;
            
            while (k < length) {
                const kValue = o[k];
                if (predicate.call(arg, kValue, k, o))
                    return kValue;
                
                k++;
            }

            return undefined;
        },
        configurable: true,
        writable: true
    });
}