/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

window.onload = function() {
    let submitting = false;

    // Change password action
    document.getElementById('new_password-form').addEventListener('submit', function(e) {
        e.preventDefault();

        if (submitting)
            return;

        const password = document.getElementById('input-password');
        const password2 = document.getElementById('input-password2');

        const passwordFeedback = password.nextElementSibling;
        const password2Feedback = password2.nextElementSibling;

        if (!password.value || (!password2.value || password2.value != password.value)) {
            if (!password.value) {
                password.classList.add('is-invalid');
                passwordFeedback.innerText = 'El campo de contraseña no puede estar vacio';
            } else {
                password.classList.remove('is-invalid');
                passwordFeedback.innerText = '';
            }
            
            if (!password2.value) {
                password2.classList.add('is-invalid');
                password2Feedback.innerText = 'El campo de confirmar contraseña no puede estar vacio';
            } else if (password2.value != password.value) {
                password2.classList.add('is-invalid');
                password2Feedback.innerText = 'Las contraseñas no coinciden'; 
            } else {
                password2.classList.remove('is-invalid');
                password2Feedback.innerText = '';
            }

            return;
        }

        submitting = true;
        $.ajax({
            type: 'POST',
            url: '/api/users/new_password',
            data: {
                uid: document.getElementById('input-uid').value,
                password: password.value,
                password2: password2.value
            },
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    window.location.href = data.sendTo;
                    return;
                }

                if (data.errors.password) {
                    password.classList.add('is-invalid');
                    data.errors.password.forEach(function(error) {
                        passwordFeedback.innerHTML = '<p>' + error + '</p>';
                    })
                } else {
                    password.classList.remove('is-invalid');
                    passwordFeedback.innerText = '';
                }
                
                if (data.errors.password2) {
                    password2.classList.add('is-invalid');
                    data.errors.password2.forEach(function(error) {
                        password2Feedback.innerHTML = '<p>' + error + '</p>';
                    })
                } else {
                    password2.classList.remove('is-invalid');
                    password2Feedback.innerText = '';
                }
                
                submitting = false;
            },
            complete: function() {
                submitting = false;
            }
        });
    });
};
