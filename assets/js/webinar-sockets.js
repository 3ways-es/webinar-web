/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-sockets
 */

class WebinarSocket {
    /**
     * Socket host
     * 
     * @type {string}
     */
    host;

    /**
     * Socket port
     * 
     * @type {number}
     */
    port;

    /**
     * Debug mode bool
     * true = show exception logs
     * 
     * @type {boolean}
     */
    debugMode;

    /**
     * Socket connection
     * 
     * @type {WebSocket}
     */
    socket;

    /**
     * Socket open listener function
     * 
     * @type {void}
     */
    onConnect;

    /**
     * Socket errors listener function
     * 
     * @type {void}
     */
    onError;

    /**
     * Socket messages listener function
     * 
     * @type {void}
     */
    onMessage;
    
    /**
     * Socket close listener function
     * 
     * @type {void}
     */
    onClose;

    /**
     * Packets list
     * 
     * @type {[{ id: number, invokable: void }]}
     */
    packets;

    /**
     * Class constructor
     * 
     * @constructs WebinarSocket
     * 
     * @param {string} host
     * @param {number} [port]
     * @param {boolean} [debug_mode]
     */
    constructor(host, port = 0, debug_mode = false) {
        this.host = host;
        this.port = port;
        this.debugMode = debug_mode;
        this.packets = [ ];

        // Default webinar-socket function to execute
        // on receive messages from server
        const self = this;
        this.onMessage = function(e) {
            try {
                const packetData = JSON.parse(e.data);
                self.executePacket(packetData.id, packetData.data);
            } catch (ex) {
                self.logError(ex);
            }
        };
    }

    /**
     * Connect to socket server
     */
    createSocket() {
        this.socket = new WebSocket(`${((window.location.protocol == 'https:') ? 'wss' : 'ws')}://${((this.port > 0) ? `${this.host}:${this.port}` : this.host)}`);
        this.socket.onopen = this.onConnect;
        this.socket.onerror = this.onError;
        this.socket.onmessage = this.onMessage;
        this.socket.onclose = this.onClose;
    }

    /**
     * Send message to server
     * 
     * @param {string} data 
     */
    send(data) {
        this.socket.send(data);
    }

    /**
     * Set socket open listener function
     * 
     * @param {void} callback
     */
    setOnConnect(callback) {
        this.onConnect = callback;
    }

    /**
     * Set socket errors listener function
     * 
     * @param {void} callback
     */
    setOnError(callback) {
        this.onError = callback;
    }

    /**
     * Set socket messages listener function
     * 
     * @param {void} callback
     */
    setOnMessage(callback) {
        this.onMessage = callback;
    }
    
    /**
     * Set socket close listener function
     * 
     * @param {void} callback
     */
    setOnClose(callback) {
        this.onClose = callback;
    }

    /**
     * Add packet to list
     * 
     * @param {number} id 
     * @param {void} callback 
     */
    addPacket(id, callback) {
        this.packets = [
            {
                id: id,
                invokable: callback
            },
            ...this.packets
        ];

        this.logInfo(`Registered packet ${id}`);
    }

    /**
     * Execute packet invokable by id
     * 
     * @param {number} id
     * @param {?[]} data
     */
    executePacket(id, data = null) {
        const packet = this.packets.find(function (packet) { return packet.id == id; });
        if (!packet) {
            this.logError(`Packet ${id} not found!`);
            return;
        }

        this.logInfo(`Received packet ${id}`);
        packet.invokable(data);
    }

    /**
     * Write error to console logs
     * 
     * @param {string} message 
     */
    logError(message) {
        if (!this.debugMode) return;
        
        console.error(message);
    }
    
    /**
     * Write info to console logs
     * 
     * @param {string} message 
     */
    logInfo(message) {
        if (!this.debugMode) return;
        
        console.log(message);
    }
}
