/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    let submittingLogin = false;
    let submittingRegister = false;

    // Login button action
    if (document.getElementById('login-button'))
        document.getElementById('login-button').addEventListener('click', function() {
            $('#login-modal').modal('show');
        });

    // Login action
    if (document.getElementById('login-form'))
        document.getElementById('login-form').addEventListener('submit', function(e) {
            e.preventDefault();

            if (submittingLogin)
                return;
            
            const email = document.getElementById('input-email');
            const password = document.getElementById('input-password');

            const emailFeedback = email.nextElementSibling;
            const passwordFeedback = password.nextElementSibling;

            if (!email.value || !password.value) {
                if (!email.value) {
                    email.classList.add('is-invalid');
                    emailFeedback.innerText = 'El campo de email no puede estar vacio';
                } else {
                    email.classList.remove('is-invalid');
                    emailFeedback.innerText = '';
                }

                if (!password.value) {
                    password.classList.add('is-invalid');
                    passwordFeedback.innerText = 'El campo de contraseña no puede estar vacio';
                } else {
                    password.classList.remove('is-invalid');
                    passwordFeedback.innerText = '';
                }

                return;
            }

            submittingLogin = true;
            $.ajax({
                type: 'POST',
                url: '/api/auth/signin',
                data: {
                    email: email.value,
                    password: password.value
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        window.location.href = data.sendTo;
                        return;
                    }

                    if (data.errors.email) {
                        email.classList.add('is-invalid');
                        data.errors.email.forEach(function(error) {
                            emailFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        email.classList.remove('is-invalid');
                        emailFeedback.innerText = '';
                    }

                    if (data.errors.password) {
                        password.classList.add('is-invalid');
                        data.errors.password.forEach(function(error) {
                            passwordFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        password.classList.remove('is-invalid');
                        passwordFeedback.innerText = '';
                    }
                    
                    submittingLogin = false;
                },
                complete: function() {
                    submittingLogin = false;
                }
            });
        });
    
    // Register button action
    if (document.getElementById('register-button'))
        document.getElementById('register-button').addEventListener('click', function() {
            $('#register-modal').modal('show');
        });

    // Register action
    if (document.getElementById('register-form'))
        document.getElementById('register-form').addEventListener('submit', function(e) {
            e.preventDefault();
            
            if (submittingRegister)
                return;
            
            const name = document.getElementById('input-name');
            const surname = document.getElementById('input-surname');
            const surname2 = document.getElementById('input-surname2');
            const nif = document.getElementById('input-nif');
            const nationality = document.getElementById('input-nationality');
            const remail = document.getElementById('input-remail');
            const rpassword = document.getElementById('input-rpassword');
            const password2 = document.getElementById('input-password2');
            const speciality = document.getElementById('input-speciality');
            const workplace = document.getElementById('input-workplace');
            const province = document.getElementById('input-province');
            const town = document.getElementById('input-town');
            const zip = document.getElementById('input-zip');
            const ppolicy = document.getElementById('input-ppolicy');
            const newsletters = document.getElementById('input-newsletters');
            const recaptcha = document.getElementById('input-recaptcha');
            
            const nameFeedback = name.nextElementSibling;
            const surnameFeedback = surname.nextElementSibling;
            const surname2Feedback = surname2.nextElementSibling;
            const nifFeedback = (nif ? nif.nextElementSibling : null);
            const nationalityFeedback = (nationality ? nationality.nextElementSibling : null);
            const remailFeedback = remail.nextElementSibling;
            const rpasswordFeedback = rpassword.nextElementSibling;
            const password2Feedback = password2.nextElementSibling;
            const specialityFeedback = (speciality ? speciality.nextElementSibling : null);
            const workplaceFeedback = workplace.nextElementSibling;
            const provinceFeedback = province.nextElementSibling;
            const townFeedback = town.nextElementSibling;
            const zipFeedback = zip.nextElementSibling;
            const ppolicyFeedback = ppolicy.nextElementSibling.nextElementSibling;
            const recaptchaFeedback = recaptcha.nextElementSibling;

            if (!name.value || !surname.value || !surname2.value || (nif && !nif.value) || (nationality && !nationality.value) || !remail.value || !rpassword.value || (!password2.value || password2.value != rpassword.value) || (speciality && !speciality[speciality.selectedIndex].value) || !workplace.value || !province[province.selectedIndex].value || !town.value || !zip.value || !ppolicy.checked || !grecaptcha.getResponse()) {
                if (!name.value) {
                    name.classList.add('is-invalid');
                    nameFeedback.innerText = 'El campo de nombre no puede estar vacío';
                } else {
                    name.classList.remove('is-invalid');
                    nameFeedback.innerText = '';
                }

                if (!surname.value) {
                    surname.classList.add('is-invalid');
                    surnameFeedback.innerText = 'El campo de primer apellido no puede estar vacío';
                } else {
                    surname.classList.remove('is-invalid');
                    surnameFeedback.innerText = '';
                }

                if (!surname2.value) {
                    surname2.classList.add('is-invalid');
                    surname2Feedback.innerText = 'El campo de segundo apellido no puede estar vacío';
                } else {
                    surname2.classList.remove('is-invalid');
                    surname2Feedback.innerText = '';
                }

                if (nif && !nif.value) {
                    nif.classList.add('is-invalid');
                    nifFeedback.innerText = 'El campo de nif no puede estar vacío';
                } else if (nif) {
                    nif.classList.remove('is-invalid');
                    nifFeedback.innerText = '';
                }

                if (nationality && !nationality.value) {
                    nationality.classList.add('is-invalid');
                    nationalityFeedback.innerText = 'El campo de nacionalidad no puede estar vacío';
                } else if (nationality) {
                    nationality.classList.remove('is-invalid');
                    nationalityFeedback.innerText = '';
                }

                if (!remail.value) {
                    remail.classList.add('is-invalid');
                    remailFeedback.innerText = 'El campo de email no puede estar vacío';
                } else {
                    remail.classList.remove('is-invalid');
                    remailFeedback.innerText = '';
                }

                if (!rpassword.value) {
                    rpassword.classList.add('is-invalid');
                    rpasswordFeedback.innerText = 'El campo de contraseña no puede estar vacío';
                } else {
                    rpassword.classList.remove('is-invalid');
                    rpasswordFeedback.innerText = '';
            }

                if (!password2.value) {
                    password2.classList.add('is-invalid');
                    password2Feedback.innerText = 'El campo de confirmar contraseña no puede estar vacío';
                } else if (password2.value != rpassword.value) {
                    password2.classList.add('is-invalid');
                    password2Feedback.innerText = 'Las contraseñas no coinciden'; 
                } else {
                    password2.classList.remove('is-invalid');
                    password2Feedback.innerText = '';
                }

                if (speciality && !speciality[speciality.selectedIndex].value) {
                    speciality.classList.add('is-invalid');
                    specialityFeedback.innerText = 'El campo de especialidad no puede estar vacío';
                } else if (speciality) {
                    speciality.classList.remove('is-invalid');
                    specialityFeedback.innerText = '';
                }

                if (!workplace.value) {
                    workplace.classList.add('is-invalid');
                    workplaceFeedback.innerText = 'El campo de centro de trabajo no puede estar vacío';
                } else {
                    workplace.classList.remove('is-invalid');
                    workplaceFeedback.innerText = '';
                }

                if (!province[province.selectedIndex].value) {
                    province.classList.add('is-invalid');
                    provinceFeedback.innerText = 'El campo de provincia no puede estar vacío';
                } else {
                    province.classList.remove('is-invalid');
                    provinceFeedback.innerText = '';
                }

                if (!town.value) {
                    town.classList.add('is-invalid');
                    townFeedback.innerText = 'El campo de población no puede estar vacío';
                } else {
                    town.classList.remove('is-invalid');
                    townFeedback.innerText = '';
                }

                if (!zip.value) {
                    zip.classList.add('is-invalid');
                    zipFeedback.innerText = 'El campo de código postal/zip no puede estar vacío';
                } else {
                    zip.classList.remove('is-invalid');
                    zipFeedback.innerText = '';
                }

                if (!ppolicy.checked) {
                    ppolicy.classList.add('is-invalid');
                    ppolicyFeedback.innerText = 'Debe aceptar la política de privacidad y términos de uso';
                } else {
                    ppolicy.classList.remove('is-invalid');
                    ppolicyFeedback.innerText = '';
                }

                if (!grecaptcha.getResponse()) {
                    recaptcha.classList.add('is-invalid');
                    recaptchaFeedback.innerText = 'Debe realizar la comprobación del captcha';
                } else {
                    recaptcha.classList.remove('is-invalid');
                    recaptchaFeedback.innerText = '';
                }

                return;
            }

            submittingRegister = true;
            $.ajax({
                type: 'POST',
                url: '/api/auth/signup',
                data: {
                    name: name.value,
                    surname: surname.value,
                    surname2: surname2.value,
                    nif: (nif ? nif.value : null),
                    nationality: (nationality ? nationality.value : null),
                    remail: remail.value,
                    rpassword: rpassword.value,
                    password2: password2.value,
                    speciality: (speciality ? speciality.value : null),
                    workplace: workplace.value,
                    province: province.value,
                    town: town.value,
                    zip: zip.value,
                    ppolicy: ppolicy.checked,
                    newsletters: newsletters.checked,
                    'g-recaptcha-response': grecaptcha.getResponse()
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        window.location.href = data.sendTo;
                        return;
                    }

                    if (data.errors.name) {
                        name.classList.add('is-invalid');
                        data.errors.name.forEach(function(error) {
                            nameFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        name.classList.remove('is-invalid');
                        nameFeedback.innerText = '';
                    }
        
                    if (data.errors.surname) {
                        surname.classList.add('is-invalid');
                        data.errors.surname.forEach(function(error) {
                            surnameFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        surname.classList.remove('is-invalid');
                        surnameFeedback.innerText = '';
                    }
        
                    if (data.errors.surname2) {
                        surname2.classList.add('is-invalid');
                        data.errors.surname2.forEach(function(error) {
                            surname2Feedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        surname2.classList.remove('is-invalid');
                        surname2Feedback.innerText = '';
                    }
        
                    if (data.errors.nif) {
                        nif.classList.add('is-invalid');
                        data.errors.nif.forEach(function(error) {
                            nifFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        nif.classList.remove('is-invalid');
                        nifFeedback.innerText = '';
                    }
        
                    if (data.errors.nationality) {
                        nationality.classList.add('is-invalid');
                        data.errors.nationality.forEach(function(error) {
                            nationalityFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        nationality.classList.remove('is-invalid');
                        nationalityFeedback.innerText = '';
                    }
        
                    if (data.errors.remail) {
                        remail.classList.add('is-invalid');
                        data.errors.remail.forEach(function(error) {
                            remailFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        remail.classList.remove('is-invalid');
                        remailFeedback.innerText = '';
                    }
        
                    if (data.errors.rpassword) {
                        rpassword.classList.add('is-invalid');
                        data.errors.rpassword.forEach(function(error) {
                            rpasswordFeedback.innerHTML = '<p>' + error + '</p>';
                    })
                    } else {
                        rpassword.classList.remove('is-invalid');
                        rpasswordFeedback.innerText = '';
                }
        
                    if (data.errors.password2) {
                        password2.classList.add('is-invalid');
                        data.errors.password2.forEach(function(error) {
                            password2Feedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        password2.classList.remove('is-invalid');
                        password2Feedback.innerText = '';
                    }
        
                    if (data.errors.speciality) {
                        speciality.classList.add('is-invalid');
                        data.errors.speciality.forEach(function(error) {
                            specialityFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        speciality.classList.remove('is-invalid');
                        specialityFeedback.innerText = '';
                    }
        
                    if (data.errors.workplace) {
                        workplace.classList.add('is-invalid');
                        data.errors.workplace.forEach(function(error) {
                            workplaceFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        workplace.classList.remove('is-invalid');
                        workplaceFeedback.innerText = '';
                    }
        
                    if (data.errors.province) {
                        province.classList.add('is-invalid');
                        data.errors.province.forEach(function(error) {
                            provinceFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        province.classList.remove('is-invalid');
                        provinceFeedback.innerText = '';
                    }
        
                    if (data.errors.town) {
                        town.classList.add('is-invalid');
                        data.errors.town.forEach(function(error) {
                            townFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        town.classList.remove('is-invalid');
                        townFeedback.innerText = '';
                    }
        
                    if (data.errors.zip) {
                        zip.classList.add('is-invalid');
                        data.errors.zip.forEach(function(error) {
                            zipFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        zip.classList.remove('is-invalid');
                        zipFeedback.innerText = '';
                    }
        
                    if (data.errors.ppolicy) {
                        ppolicy.classList.add('is-invalid');
                        data.errors.ppolicy.forEach(function(error) {
                            ppolicyFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        ppolicy.classList.remove('is-invalid');
                        ppolicyFeedback.innerText = '';
                    }
                    
                    if (data.errors.recaptcha) {
                        recaptcha.classList.add('is-invalid');
                        data.errors.recaptcha.forEach(function(error) {
                            recaptchaFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        recaptcha.classList.remove('is-invalid');
                        recaptchaFeedback.innerText = '';
                    }

                    submittingRegister = false;
                },
                complete: function() {
                    submittingRegister = false;
                }
            });
        });
});
