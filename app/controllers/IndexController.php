<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\Especialidad;
use App\Models\Provincia;

class IndexController extends Controller
{
    public function index($request)
    {
      if (config('site.mantenimiento')){
        return view('proximamente');
      }

      $this->permissions([ 'guest' ]);

      return view('index', [
          'specialities' => Especialidad::all([ ['borrado', '=', 0] ], null),
          'provinces' => Provincia::all([ ['borrado', '=', 0] ], null)
      ]);
  }

  public function preview($request)
  {
    $this->permissions([ 'guest' ]);

    return view('index', [
        'specialities' => Especialidad::all([ ['borrado', '=', 0] ], null),
        'provinces' => Provincia::all([ ['borrado', '=', 0] ], null)
    ]);
  }
}
