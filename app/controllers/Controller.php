<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

class Controller
{
    /**
     * Check permissions to show the page
     *
     * @param array $permissions
     * @param bool $api
     * 
     * @return void
     */
    public function permissions($permissions, $api = false)
    {
        $load_view = true;

        foreach ($permissions as $permission):
            switch ($permission):
                case 'auth':
                    $load_view = auth()->loggedIn();
                    
                    if (!$load_view && !$api):
                        header('Location: ' . config('app.url'));
                        exit;
                    endif;
                break;

                case 'guest':
                    $load_view = !auth()->loggedIn();

                    if (!$load_view && !$api):
                        header('Location: ' . route('seleccion')->path);
                        exit;
                    endif;
                break;
            endswitch;
        endforeach;

        if (!$load_view) die($api ? json_encode([ 'success' => false, 'message' => 'Unauthorized' ]) : view('error'));
    }
}
