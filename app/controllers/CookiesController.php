<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

class CookiesController extends Controller
{
    public function getCookie($name)
    {
        $name = strip_tags($name);
        $data = [ 'exists' => false, 'value' => null ];
        
        if (!$name || !isset($_COOKIE[$name]))
            goto Result;

        $data['exists'] = true;
        $data['value'] = $_COOKIE[$name];

        Result:
        return $data;
    }
    
    public function addCookie($request)
    {
        $name = strip_tags($request->input->cname);
        $value = strip_tags($request->input->cvalue);
        $data = [ 'sucess' => false ];

        if (!$name || !$value)
        {
            if (!$name)
                $data['errors']['name'] = 'No cookie name provided';
            
            if (!$value)
                $data['errors']['value'] = 'No cookie value provided';

            goto Result;
        }

        header('X-Frame-Options: ALLOW-FROM ' . config('app.url'));
        header("Set-Cookie:{$name}={$value}; secure; httpOnly");

        $data['success'] = true;
        $data['message'] = 'Cookie created successfully';

        Result:
        return $data;
    }
}
