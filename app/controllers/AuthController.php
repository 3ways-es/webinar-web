<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\User;
use App\Models\LoginHistorial;
use \PHPMailer;

class AuthController extends Controller
{
    public function signin($request)
    {
        $this->permissions([ 'guest' ], true);

        $email = $request->input->email;
        $password = $request->input->password;
        $data = [ 'success' => false ];

        if (!$email || !$password):
            if (!$email)
                $data['errors']['email'][] = 'El campo de email no puede estar vacio';
            
            if (!$password)
                $data['errors']['password'][] = 'El campo de contraseña no puede estar vacio';

            goto Result;
        endif;

        // Data validation process
        $user_by_email = User::first('*', [ ['email', $email] ]);
        if (!$user_by_email):
            $data['errors']['email'][] = 'No hay usuarios registrados con este email';
            goto Result;
        endif;
        
        $login_tries = LoginHistorial::count([ ['login', $email] ]);
        if ($login_tries > 10):
            $data['errors']['email'][] = 'Ha superado el número de intentos permitidos, por favor intentelo más tarde';
            goto Result;
        endif;

        $hash = auth()->checkHash($password, $user_by_email);
        if (!$hash->isValid()):
            if ($login_tries == 10):
                $login_historial = LoginHistorial::first('fecha', [ ['login', $email] ], [ ['id', 'DESC'] ]);
                $current_date = new DateTime(date("Y-m-d H:i:s"));
                $login_date = new DateTime($login_historial->fecha);
                $interval = $current_date->diff($login_date);
    
                if ($interval->format('%i') < 6):
                    $data['errors']['email'][] = 'Ha superado el límite de intentos fallidos, debe esperar 5 minutos para volver a intenterlo';
                    goto Result;
                else:
                    $this->removeLoginHistorialesByEmail($email);
                endif;
            endif;

            $login_historial = new LoginHistorial;
            $login_historial->login = $email;
            $login_historial->ip = $_SERVER['REMOTE_ADDR'];
            $login_historial->fecha = date("Y-m-d H:i:s");
            $login_historial->success = 0;
            $login_historial->save();

            $data['errors']['password'][] = 'La contraseña no es correcta';
            goto Result;
        endif;

        $this->removeLoginHistorialesByEmail($email);
        
        $data['success'] = true;
        $data['message'] = 'Authorized login';
        $data['sendTo'] = route('seleccion')->path;

        auth()->setUser($user_by_email, $password);
        
        Result:
        return $data;
    }

    private function removeLoginHistorialesByEmail($email)
    {
        $login_historiales = LoginHistorial::all([ ['login', $email] ], null);
        foreach ($login_historiales as $login_historial)
            $login_historial->remove();
    }

    public function signup($request)
    {
        $this->permissions([ 'guest' ], true);

        $name = $request->input->name;
        $surname = $request->input->surname;
        $surname2 = $request->input->surname2;
        $nif = $request->input->nif;
        $nationality = $request->input->nationality;
        $remail = $request->input->remail;
        $rpassword = $request->input->rpassword;
        $password2 = $request->input->password2;
        $speciality = $request->input->speciality;
        $workplace = $request->input->workplace;
        $province = $request->input->province;
        $town = $request->input->town;
        $zip = $request->input->zip;
        $ppolicy = ($request->input->ppolicy == 'true');
        $newsletters = ($request->input->newsletters == 'true');
        $recaptcha = $request->input->{'g-recaptcha-response'};

        $data = [ 'success' => false ];
        $user_by_email = User::first('*', [ ['email', $remail] ]);

        if (!$name || !$surname || !$surname2 || (show_register_extra_field('nif') && !$nif) || (show_register_extra_field('nacionalidad') && !$nationality) || (!$remail || $user_by_email) || !$rpassword || (!$password2 || ($rpassword != $password2)) || (show_register_extra_field('especialidad') && !$speciality) || !$workplace || !$province || !$town || !$zip || !$ppolicy || !$recaptcha):
            if (!$name)
                $data['errors']['name'][] = 'El campo de nombre no puede estar vacío';

            if (!$surname)
                $data['errors']['surname'][] = 'El campo de primer apellido no puede estar vacío';

            if (!$surname2)
                $data['errors']['surname2'][] = 'El campo de segundo apellido no puede estar vacío';

            if (show_register_extra_field('nif') && !$nif)
                $data['errors']['nif'][] = 'El campo de nif no puede estar vacío';

            if (show_register_extra_field('nacionalidad') && !$nationality)
                $data['errors']['nationality'][] = 'El campo de nacionalidad no puede estar vacío';

            if (!$remail)
                $data['errors']['remail'][] = 'El campo de email no puede estar vacío';
            elseif ($user_by_email)
                $data['errors']['remail'][] = 'Ya existe un usuario con ese email';

            if (!$rpassword)
                $data['errors']['rpassword'][] = 'El campo de contraseña no puede estar vacío';

            if (!$password2)
                $data['errors']['password2'][] = 'El campo de confirmar contraseña no puede estar vacío';
            elseif ($rpassword != $password2)
                $data['errors']['password2'][] = 'Las contraseñas no coinciden';

            if (show_register_extra_field('especialidad') && !$speciality)
                $data['errors']['speciality'][] = 'El campo de especialidad no puede estar vacío';

            if (!$workplace)
                $data['errors']['workplace'][] = 'El campo de centro de trabajo no puede estar vacío';

            if (!$province)
                $data['errors']['province'][] = 'El campo de provincia no puede estar vacío';

            if (!$town)
                $data['errors']['town'][] = 'El campo de población no puede estar vacío';

            if (!$zip)
                $data['errors']['zip'][] = 'El campo de código postal/zip no puede estar vacío';

            if (!$ppolicy)
                $data['errors']['ppolicy'][] = 'Debe aceptar la política de privacidad y términos de uso';

            if (!$recaptcha)
                $data['errors']['recaptcha'][] = 'Debe realizar la comprobación del captcha';

            goto Result;
        endif;

        // Captcha validation
        $catpcha_response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('recaptcha.secret_key') . '&response=' . $recaptcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']), true);
        if (!$catpcha_response['success']):
            $data['errors']['recaptcha'][] = 'El captcha es incorrecto';
            goto Result;
        endif;

        $hash = auth()->createHash($rpassword);
        $user = new User;
        $user->name = $name;
        $user->surname = "{$surname} {$surname2}";
        $user->email = $remail;
        $user->password_s = $hash->salt;
        $user->password_h = $hash->hash;
        $user->id_provincia = $province;
        $user->city = $town;
        $user->codigo_postal = $zip;
        $user->centro_trabajo = $workplace;
        $user->especialidad = ($speciality || 1);
        $user->codigo_delegado = '';
        $user->acepto_publicar = ($newsletters ? 1 : 0);
        $user->acepto_politica = ($ppolicy ? 1 : 0);
        $user->date_add = date("Y-m-d H:i:s");
        $user->date_mod = date("Y-m-d H:i:s");
        if ($nationality) $user->nacionalidad = $nationality;
        if ($nif) $user->nif = $nif;
        $user->save();

        send_mail('registro', $user->email, ('Confirmación inscripción ' . config('app.name')), [
            '[[NAME]]' => $user->name,
            '[[WEB_ROOT]]' => config('app.url'),
            '[[WEB_ROOT_MAILS]]' => config('app.url'),
            '[[USUARIO]]' => $user->email,
            '[[PASSWORD]]' => $rpassword
        ]);
        
        $data['success'] = true;
        $data['message'] = 'User registered successfully';
        $data['sendTo'] = route('seleccion')->path;

        auth()->setUser($user, $rpassword);
        
        Result:
        return $data;
    }

    public function logout()
    {
        $this->permissions([ 'auth' ]);
        
        auth()->logout();
        header('Location: ' . config('app.url'));
    }
}
