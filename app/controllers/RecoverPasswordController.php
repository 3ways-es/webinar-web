<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\User;
use \PHPMailer;

class RecoverPasswordController extends Controller
{
    public function index($request)
    {
        $this->permissions([ 'guest' ]);

        return view('recover', [
            'success' => ($request->input->success ?? false)
        ]);
    }

    public function recover($request)
    {
        $this->permissions([ 'guest' ], true);

        $email = $request->input->email;
        $recaptcha = $request->input->{'g-recaptcha-response'};

        $data = [ 'success' => false ];
        $user_by_email = User::first('*', [ ['email', $email] ]);

        if ((!$email || !$user_by_email) || !$recaptcha):
            if (!$email)
                $data['errors']['email'][] = 'El campo de email no puede estar vacío';
            elseif (!$user_by_email)
                $data['errors']['email'][] = 'No hay usuarios registrados con ese email';

            if (!$recaptcha)
                $data['errors']['recaptcha'][] = 'Debe realizar la comprobación del captcha';

            goto Result;
        endif;

        // Captcha validation
        $catpcha_response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('recaptcha.secret_key') . '&response=' . $recaptcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']), true);
        if (!$catpcha_response['success']):
            $data['errors']['recaptcha'][] = 'El captcha es incorrecto';
            goto Result;
        endif;

        send_mail('recuperar', $user_by_email->email, ('Recuperar contraseña ' . config('app.name')), [
            '[[NAME]]' => $user_by_email->name,
            '[[WEB_ROOT]]' => config('app.url'),
            '[[WEB_ROOT_MAILS]]' => config('app.url'),
            '[[USUARIO]]' => $user_by_email->email,
            '[[KEY]]' => md5($user_by_email->id . '_&hasdsdn?_'),
            '[[ID]]' => $user_by_email->id
        ]);
        
        $data['success'] = true;
        $data['message'] = 'Recover password email was sent successfully';
        $data['sendTo'] = route('recover')->path . '?success=true';
        
        Result:
        return $data;
    }
    
    public function newPassword($uid, $key)
    {
        $this->permissions([ 'guest' ]);

        if (!$uid || (!$key || $key != md5($uid . '_&hasdsdn?_')))
            return view('error');

        return view('new_password', [
            'uid' => $uid
        ]);
    }
    
    public function changePassword($request)
    {
        $this->permissions([ 'guest' ], true);

        $uid = $request->input->uid;
        $password = $request->input->password;
        $password2 = $request->input->password2;
        $data = [ 'success' => false ];

        if (!$uid || !$password || (!$password2 || $password2 != $password)):
            if (!$uid)
                $data['errors']['password'][] = 'Error al intentar cambiar la contraseña';

            if (!$password)
                $data['errors']['password'][] = 'El campo de contraseña no puede estar vacio';

            if (!$password2)
                $data['errors']['password2'][] = 'El campo de confirmar contraseña no puede estar vacio';
            elseif ($password2 != $password)
                $data['errors']['password2'][] = 'Las contraseñas no coinciden';

            goto Result;
        endif;

        // Data validation process
        $user = User::find($uid);
        if (!$user):
            $data['errors']['password'][] = 'Error al intentar cambiar la contraseña';
            goto Result;
        endif;
        
        $hash = auth()->createHash($password);
        $user->password_h = $hash->hash;
        $user->password_s = $hash->salt;
        $user->save();
        
        $data['success'] = true;
        $data['message'] = 'Password changed succesfully';
        $data['sendTo'] = route('seleccion')->path;

        auth()->setUser($user, $password);
        
        Result:
        return $data;
    }
}
