<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\Pregunta;

class QuestionsController extends Controller
{
    public function getQuestions()
    {
        $this->permissions([ 'auth' ], true);

        return [
            'success' => true,
            'data' => auth()->user()->questions()
        ];
    }

    public function createQuestion($request)
    {
        $this->permissions([ 'auth' ], true);
        
        $message = $request->input->question;
        $audio = $request->files->audio;
        $data = [ 'success' => false ];
        
        if (!$audio):
            if (!$message || strlen($message) > 450):
                if (!$message)
                    $data['errors']['message'][] = 'El campo de pregunta no puede estar vacio';
                elseif (strlen($message) > 450)
                    $data['errors']['message'][] = 'El texto es demasiado largo';

                goto Result;
            endif;
        else:
            $dir_to_save = (__DIR__ . '/../../storage/preguntas-audios/' . date('Y-m-d'));
            if (!is_dir($dir_to_save))
                mkdir($dir_to_save, 0777, true);

            if (!move_uploaded_file($audio->tmp_name, "{$dir_to_save}/{$request->input->audio_name}.mp3")):
                $data['errors']['audio'] = 'El audio no se ha podido guardar';
                goto Result;
            endif;
        endif;

        $question = new Pregunta;
        $question->idusuariogesida = auth()->user()->id;
        $question->pregunta = $message;
        $question->fecha = date('Y-m-d H:i:s');
        $question->orden = 0;
        $question->save();
        
        $data['success'] = true;
        $data['message'] = 'Question created successfully';

        Result:
        return $data;
    }

    public function editQuestion($id, $request)
    {
        $this->permissions([ 'auth' ], true);
        
        $message = $request->input->question;
        $data = [ 'success' => false ];

        if (!$message || strlen($message) > 450):
            if (!$message)
                $data['errors']['message'][] = 'El campo de pregunta no puede estar vacio';
            elseif (strlen($message) > 450)
                $data['errors']['message'][] = 'El texto es demasiado largo';

            goto Result;
        endif;

        $question = Pregunta::first('*', [ ['idusuariogesida', auth()->user()->id], ['idpregunta', $id] ]);
        if (!$question || $question->estado != 0):
            if (!$question)
                $data['errors']['message'][] = 'Not found';
                elseif ($question->estado != 0)
                $data['errors']['message'][] = 'La pregunta no se puede editar porque está bloqueada';
            
            goto Result;
        endif;

        $question->pregunta = $message;
        $question->save();
        
        $data['success'] = true;
        $data['message'] = 'Question edited successfully';

        Result:
        return $data;
    }
    
    public function deleteQuestion($id, $request)
    {
        $this->permissions([ 'auth' ], true);
        
        $data = [ 'success' => false ];
        $question = Pregunta::first('*', [ ['idusuariogesida', auth()->user()->id], ['idpregunta', $id] ]);

        if (!$question || $question->estado != 0):
            if (!$question)
                $data['errors']['message'][] = 'Not found';
            elseif ($question->estado != 0)
                $data['errors']['message'][] = 'La pregunta no se puede eliminar porque está bloqueada';
            
            goto Result;
        endif;

        $path = __DIR__ . "/../../storage/{$question->pregunta}";
        if (is_file($path))
            unlink($path);

        $question->remove();
        
        $data['success'] = true;
        $data['message'] = 'Question removed successfully';

        Result:
        return $data;
    }
}
