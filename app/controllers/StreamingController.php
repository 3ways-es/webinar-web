<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\ControlFuente;
use App\Models\Pregunta;
use App\Models\UserTiempo;

class StreamingController extends Controller
{
    public function index($request)
    {
        $this->permissions([ 'auth' ]);
        
        $source = ControlFuente::first('*', [ ['idControlFuente', '!=', 6], ['activo', 1] ]);
        if (!$source)
            return view('error');
        
        return view('streaming', [
            'source' => $source,
            'preguntas' => auth()->user()->questions()
        ]);
    }

    public function registerUserTiempo()
    {
        $this->permissions([ 'auth' ], true);

        $user_tiempo = new UserTiempo;
        $user_tiempo->users_id = auth()->user()->id;
        $user_tiempo->entrada = date('Y-m-d H:i:s');
        $user_tiempo->save();

        return [
            'success' => true,
            'payload' => base64_encode($user_tiempo)
        ];
    }

    public function unregisterUserTiempo($payload)
    {
        $this->permissions([ 'auth' ], true);

        $payload = json_decode(base64_decode($payload));
        $data = [ 'success' => false ];

        if (!$payload || !is_object($payload)):
            $data['message'] = 'Invalid payload!';
            goto Result;
        endif;

        $user_tiempo = UserTiempo::find($payload->id);
        if (!$user_tiempo || $user_tiempo->users_id != auth()->user()->id || $user_tiempo->salida):
            $data['message'] = 'User tiempo does not exists!';
            goto Result;
        endif;

        $user_tiempo->salida = date('Y-m-d H:i:s');
        $user_tiempo->save();

        $data['success'] = true;
        $data['message'] = 'Set salida time from user tiempo successfully!';

        Result:
            return $data;
    }

    public function getControlFuente($id)
    {
        $this->permissions([ 'auth' ], true);
        
        return [
            'success' => true,
            'payload' => ControlFuente::find($id)
        ];
    }

    private function encrypt($data)
	{
		$encryption_key = 'keyEncryptS3W';
		$secret_iv = '3ways';
		$method = 'AES-256-CBC';

		$data = trim(json_encode($data));
		$iv = substr(hash('sha256', $secret_iv), 0, 16);
		$encrypted = openssl_encrypt($data, $method, $encryption_key, 0, $iv);
		$encrypted = base64_encode($encrypted);

		return base64_encode($encrypted);
	}
}
