{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/streaming.css?{{ time() }}">
@endcontent

@content('contents')
    @add('includes.header')

    <div class="container my-5">
        <iframe id="unregister-time" class="d-none" frameborder="0"></iframe>

        <div class="row">
            <div class="col-12">
                <div class="row mx-auto">
                    <div class="source-area{{ (($source->chat == 1) ? ' col-12 col-lg-8' : ' col-12 no-chat') }}" id="video-area" data-id="{{ $source->idControlFuente }}">
                        <iframe id="video" class="frame{{ (($source->tipo == 'image') ? ' d-none' : '') }}" src="{{ (($source->tipo == 'image') ? '' : $source->source) }}" frameborder="0" allow="autoplay; fullscreen" scrolling="no" allowfullscreen></iframe>
                        <div class="content-imagen{{ (($source->tipo == 'image') ? '' : ' d-none') }}">
                            <img src="{{ (($source->tipo == 'image') ? $source->source : '') }}" alt="Source image" id="source-image">
                        </div>
                        <?php if ($source->audio): ?>
                        <audio autoplay>
                            <source src="{{ $source->audio }}" type="audio/mpeg">
                        </audio>
                        <?php endif; ?>
                    </div>

                    <div class="col-12 col-lg-4 source-area{{ (($source->chat == 1) ? '' : ' d-none') }}" id="chat-area" data-enabled="<?= ($source->chat == 1) ?>">
                        <iframe id="chat" class="frame" src="{{ $source->chat_source }}" frameborder="0" scrolling="no"></iframe>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-4 mb-2">
                <div class="row align-items-end">
                    <div class="col-8">
                        <h5 class="font-weight-bold">Preguntas de: {{ auth()->user()->name }}</h5>
                    </div>

                    <div class="col-4 text-right">
                        <img src="{{ config('app.url') }}/assets/images/actualizar.png" alt="Actualizar" id="reload-button" class="img-fluid button" />
                    </div>

                    <div class="col-12 mt-1 mb-1">
                        <hr>
                    </div>
                    
                    <div id="send-question-area" class="col-12">
                        <div class="row">
                            <div class="col-12 {{ (config('streaming.allow_voice_questions') ? 'col-lg-7 col-md-5' : 'col-lg-10 col-md-9') }} mt-2" id="question-textarea">
                                <textarea class="form-control w-100 char-counter" id="input-question" placeholder="Escribe aquí tu pregunta..."></textarea>
                                <div class="invalid-feedback"></div>

                                <span id="chars">
                                    <span id="char-count">0</span>/<span id="max-chars"></span> máximo de caracteres
                                </span>
                            </div>
                            
                            <?php if (config('streaming.allow_voice_questions')): ?>
                            <div class="col-12 col-lg-7 col-md-5 mt-2 d-none" id="question-record">
                                <div class="row">
                                    <div id="recording-list" class="col-11" style="padding-right: 1px;"></div> <!-- Fix it -->
                                    <div class="col-1" style="padding-left: 1px;"> <!-- Fix it -->
                                        <i id="close-player" class="fa fa-times-circle fa-2x cursor-pointer"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-lg-3 col-md-4 mt-2 mt-sm-3 text-center text-md-right align-self-start" id="record-voice-area">
                                <div class="row justify-content-center align-items-center">
                                    <div class="col-10 pr-0">
                                        <button class="btn btn-danger" id="record-voice">
                                            <i class="fas fa-microphone"></i> Grabar pregunta
                                        </button>
                                    </div>

                                    <div class="col-2 pl-0">
                                        <i class="fas fa-info-circle fa-lg cursor-pointer" id="voice-questions-info"></i>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-6 col-lg-2 col-md-3 mt-2 text-center text-md-right align-self-start d-none" id="recording-area">
                                <div class="row justify-content-center align-items-center">
                                    <div class="col-12">
                                        <button class="btn btn-danger btn-block" id="stop-recording">
                                            <i class="fas fa-stop"></i> Stop (<span id="record-timer">0</span> s)
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="{{ (config('streaming.allow_voice_questions') ? 'col-6' : 'col-12') }} col-lg-2 col-md-3 mt-2 text-center text-md-right align-self-start" id="send-button-area">
                                <img src="{{ config('app.url') }}/assets/images/enviar.png" alt="Enviar" id="send-question" class="img-fluid button" />
                            </div>
                        </div>
                    </div>
                    
                    <div id="edit-question-area" class="col-12">
                        <div class="row">
                            <div class="col-12 col-lg-8 col-md-6 mt-2">
                                <textarea class="form-control w-100 char-counter" id="input-edit-question" data-id="0" placeholder="Escribe aquí tu pregunta..."></textarea>
                                <div class="invalid-feedback"></div>
    
                                <span id="chars">
                                    <span id="char-count">0</span>/<span id="max-chars"></span> máximo de caracteres
                                </span>
                            </div>
    
                            <div class="col-6 col-lg-2 col-md-3 mt-2 text-center text-md-right align-self-start">
                                <img src="{{ config('app.url') }}/assets/images/enviar.png" id="send-edit-question" alt="Editar" class="img-fluid button" />
                            </div>
    
                            <div class="col-6 col-lg-2 col-md-3 mt-2 text-center text-md-right align-self-start">
                                <img src="{{ config('app.url') }}/assets/images/cancelar.png" id="cancel-question" alt="Cancelar" class="img-fluid button" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div id="questions-area" class="row">
                    <?php foreach ($preguntas as $key => $pregunta): ?>
                    <div class="col-12 mt-2">
                        <div class="question-box<?= ((($key + 1) % 2 == 0) ? ' no-bg' : '') ?>" id="question-box-<?= $pregunta->idpregunta ?>">
                            <div class="row">
                                <?php if (is_file(__DIR__ . "/../../storage/{$pregunta->pregunta}")): ?>
                                    <div class="<?= (($pregunta->estado == 0) ? 'col-10 col-md-11' : 'col-12') ?> d-flex">
                                        <span><?= ($key + 1) ?>.</span>
                                        <audio controls src="{{ config('app.url') }}/storage/<?= $pregunta->pregunta ?>" class="w-100 ml-2"></audio>
                                    </div>

                                    <?php if ($pregunta->estado == 0): ?>
                                    <div class="col-2 col-md-1 text-right">
                                        <i class="fa fa-times px-2 cursor-pointer remove-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                    </div>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <div class="<?= (($pregunta->estado == 0) ? 'col-8 col-md-10' : 'col-12')?>">
                                        <span><?= ($key + 1) ?>.</span>
                                        <span id="question-<?= $pregunta->idpregunta ?>"><?= $pregunta->pregunta ?></span>
                                    </div>
                                        
                                    <?php if ($pregunta->estado == 0): ?>
                                    <div class="col-4 col-md-2 text-right">
                                        <i class="fa fa-pencil px-2 cursor-pointer edit-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                        <i class="fa fa-times px-2 cursor-pointer remove-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>                                  
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
            <div class="col-12 mt-5"></div>
            
            <?php if (config('streaming.support_message')): ?>
            <div class="col-12 text-center fs-17">
                Si tiene algún problema con la visualización de este webinar, por favor contacte con soporte técnico (3WAYS): +34 917 540 900
            </div>
            <?php endif; ?>
        </div>
    </div>
@endcontent

@content('scripts')
    <script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
    <script src="{{ config('app.url') }}/assets/js/streaming.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/icons.min.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/check-fuente.js?{{ time() }}"></script>
@endcontent

@content('modals')
    @add('modals.change_source')
    <?php if (config('streaming.allow_voice_questions')): ?>
    @add('modals.voice_questions')
    @add('modals.compatibility')
    <?php endif; ?>
@endcontent