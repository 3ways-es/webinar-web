{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    <div class="index-view d-flex flex-column align-items-center justify-content-center">
        <div class="container">
            <img src="{{ config('app.url') }}/assets/images/logo_ppal.png" alt="Logo" class="img-fluid" />

            <div class="row justify-content-center align-items-center mt-5">
                <div class="col-6 col-md-4 col-sm-5">
                    <img src="{{ config('app.url') }}/assets/images/regristro.png" alt="Registro" id="register-button" class="img-fluid button mx-auto d-block" />
                </div>

                <div class="col-6 col-md-4 col-sm-5">
                    <img src="{{ config('app.url') }}/assets/images/accceder_ppal.png" alt="Acceder" id="login-button" class="img-fluid button mx-auto d-block" />
                </div>
            </div>
        </div>
    </div>
@endcontent

@content('scripts')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endcontent