{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="register-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <form class="row px-3" id="register-form">
                    <div class="col-6">
                        <h4 class="font-weight-bold">Registro</h4>
                    </div>
                    
                    <div class="col-6">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="col-12 col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="input-name" class="control-label font-weight-bold">Nombre*</label>
                                <input type="text" class="form-control" name="name" id="input-name" placeholder="Nombre">
                                <div class="invalid-feedback"></div>
                            </div>

                            <div class="col-12 form-group">
                                <label for="input-surname" class="control-label font-weight-bold">Primer apellido*</label>
                                <input type="text" class="form-control" name="surname" id="input-surname" placeholder="Primer apellido">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <label for="input-surname2" class="control-label font-weight-bold">Segundo apellido*</label>
                                <input type="text" class="form-control" name="surname2" id="input-surname2" placeholder="Segundo apellido">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <?php if (show_register_extra_field('nif')): ?>
                            <div class="col-12 form-group">
                                <label for="input-nif" class="control-label font-weight-bold">NIF*</label>
                                <input type="text" class="form-control" name="nif" id="input-nif" placeholder="NIF">
                                <div class="invalid-feedback"></div>
                            </div>
                            <?php endif; ?>
                            
                            <?php if (show_register_extra_field('nacionalidad')): ?>
                            <div class="col-12 form-group">
                                <label for="input-nationality" class="control-label font-weight-bold">Nacionalidad*</label>
                                <input type="text" class="form-control" name="nationality" id="input-nationality" placeholder="Nacionalidad">
                                <div class="invalid-feedback"></div>
                            </div>
                            <?php endif; ?>
                            
                            <div class="col-12 form-group">
                                <label for="input-remail" class="control-label font-weight-bold">Email*</label>
                                <input type="email" class="form-control" name="email" id="input-remail" placeholder="Email">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <label for="input-rpassword" class="control-label font-weight-bold">Contraseña* <i class="text-muted fs-12">(Mínimo 8 caracteres alfanúmericos y una mayúscula)</i></label>
                                <input type="password" class="form-control" name="password" id="input-rpassword" placeholder="Contraseña">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <label for="input-password2" class="control-label font-weight-bold">Confirmar contraseña*</label>
                                <input type="password" class="form-control" name="password2" id="input-password2" placeholder="Confirmar contraseña">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <?php if (show_register_extra_field('especialidad')): ?>
                            <div class="col-12 form-group">
                                <label for="input-speciality" class="control-label font-weight-bold">Especialidad*</label>
                                <select name="speciality" id="input-speciality" class="form-control">
                                    <option value>Seleccione una especialidad</option>

                                    <?php foreach ($specialities as $speciality): ?>
                                    <option value="<?= $speciality->id ?>"><?= $speciality->especialidad ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback"></div>
                            </div>
                            <?php endif; ?>

                            <div class="col-12 form-group">
                                <label for="input-workplace" class="control-label font-weight-bold">Centro de trabajo*</label>
                                <input type="text" class="form-control" name="workplace" id="input-workplace" placeholder="Centro de trabajo">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="input-province" class="control-label font-weight-bold">Provincia*</label>
                                <select name="province" id="input-province" class="form-control">
                                    <option value>Seleccione una provincia</option>
                                    
                                    <?php foreach ($provinces as $province): ?>
                                    <option value="<?= $province->id ?>"><?= $province->provincia ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <label for="input-town" class="control-label font-weight-bold">Población*</label>
                                <input type="text" class="form-control" id="input-town" placeholder="Población">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <label for="input-zip" class="control-label font-weight-bold">Código Postal/ZIP*</label>
                                <input type="text" class="form-control" id="input-zip" placeholder="Código postal/zip">
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <div class="g-recaptcha" id="input-recaptcha" data-sitekey="{{ config('recaptcha.site_key') }}"></div>
                                <div class="invalid-feedback"></div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <span class="text-muted fs-sm-12">* Campos obligatorios</span>
                            </div>

                            <div class="col-12 form-group">
                                <label class="text-muted fs-sm-12">El contenido de esta página web está exclusivamente dirigido a profesionales sanitarios de España con capacidad de prescribir o dispensar medicamentos, por lo que se requiere una formación especializada para su correcta interpretación. Al cumplimentar los datos de este registro, certifico que soy un profesional sanitario de España con capacidad de prescribir o dispensar medicamentos.</label>
                            </div>
                            
                            <div class="col-12 form-group">
                                <div class="form-check text-muted fs-sm-12">
                                    <input type="checkbox" name="ppolicy" id="input-ppolicy" class="form-check-input">
                                    <label class="form-check-label" for="input-ppolicy">Confirmo que he leído y entiendo la <a target="_blank" href="{{ route('privacy_policy')->path }}">Política de Protección de Datos</a> y que acepto las <a target="_blank" href="{{ route('conditions_of_use')->path }}">Condiciones de Uso</a>.</label>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            
                            <div class="col-12 form-group">
                                <div class="form-check text-muted fs-sm-12">
                                    <input type="checkbox" id="input-newsletters" name="newsletters" class="form-check-input">
                                    <label class="form-check-label" for="input-newsletters">Deseo recibir, por medios electrónicos, contenidos científicos/médicos que incluyan materiales y recursos útiles para mi práctica clínica. Estos contenidos pueden estar personalizados a mis áreas profesionales e intereses. Algunos de ellos pueden tener un carácter promocional o no (por ejemplo, newsletters, invitaciones a eventos o encuestas).<br /><br />Si no desea recibir más comunicaciones comerciales electrónicas, puede dar de baja su email <a href="{{ (config('app.allow_unsuscribe') ? route('unsuscribe')->path : 'mailto:admin@s3w.es') }}">aquí</a>.</label>
                                </div>
                            </div>
                            
                            <div class="col-12 col-lg-7 offset-lg-5 form-group mt-3 text-center text-left-lg">
                                <button type="submit" class="p-0 bg-transparent border-0">
                                    <img src="{{ config('app.url') }}/assets/images/continuar.png" alt="Acceder" class="img-fluid button d-block mx-auto" />
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>