{-- @author Rafa Rivas <rafa.rivas@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="register-success-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header my-3">
        <h5 class="modal-title">Registro completado con éxito</h5>
        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body container">
       <div class="col-12">
          Se ha registrado correctamente. Para continuar pulse en el siguiente botón.
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-12 fs-sm-14">
          <div class="row">
            <div class="col-12 col-sm-5 mx-auto">
              <button id="register-continue" class="p-0 bg-transparent border-0">
                <img src="{{ config('app.url') }}/assets/images/continuar.png" alt="Continuar" class="img-fluid button d-block mx-auto" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>