{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="voice_questions-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <div class="row px-3">
                    <div class="col-8 align-self-center">
                        <h4 class="font-weight-bold">Cómo grabar preguntas de audio</h4>
                    </div>
                    
                    <div class="col-4">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row px-3 mt-4">
                    <div class="col-12">
                        <p>Para grabar preguntas de audio debe tener en cuenta lo siguiente:</p>
                        <ul>
                            <li>Las duración máxima del audios es de 20 segundos.</li>
                            <li>Para empezar a grabar una pregunta pulse el botón <i class="fa fa-microphone"></i> Grabar Pregunta.</li>
                            <li>Si es la primera vez que va a grabar, su navegador le pedirá acceso al micrófono. Acepte, ya que en caso contrario no podrá grabar su pregunta.</li>
                            <li>Una vez concecido el permiso, puede empezar a grabar su pregunta.</li>
                            <li>Puede parar de grabar su pregunta cuando desee pulsando el boton de <i class="fa fa-stop"></i> Stop que aparecerá en lugar del botón de <i class="fa fa-microphone"></i> Grabar Pregunta, siempre que no se hayan superado los 20 segundos, en este caso la grabación se detendrá automáticamente.</li>
                            <li>Podrá ver el tiempo que le queda de grabación reflejado en el botón <i class="fa fa-stop"></i> Stop.</li>
                            <li>Al terminar de grabar su pregunta, podrá escucharla antes de enviarla. Le aparecerá un reproductor en el lugar donde antes podía escribir su pregunta.</li>
                            <li>Si no esta conforme con su grabación puede volver a realizar una nueva pulsando el botón de <i class="fa fa-microphone"></i> Grabar Pregunta. Al pulsar el botón no será posible recuperar el anterior audio.</li>
                            <li>Si prefiere realizar una pregunta escrita, puede pulsar la X que aparece a la derecha del reproductor y podrá volver a escribir su pregunta. Como en el caso anterior, si pulsa el botón X, no podrá recuperar el audio y deberá volver a grabar otro.</li>
                            <li>Si está satisfecho/a con la pregunta grabada, pulse el botón de Enviar.</li>
                        </ul>

                        <p>Recomendaciones</p>
                        <ul>
                            <li>Si es posible, grabe sus preguntas con cascos o auriculares.</li>
                            <li>Si no dispone de ellos, baje el volumen de la retransmisión que está viendo o paúsela. De esta forma solo se grabará su voz y no el audio de la retransmisión.</li>
                        </ul>
                    </div>

                    <div class="col-12 mt-4 text-right">
                        <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                            Seguir navegando
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>