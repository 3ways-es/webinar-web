{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                <h3 class="font-weight-bold text-dark-green text-uppercase">Nueva contraseña</h3>
            </div>
            <div class="col-12">
                <p class="text-dark-green">Introduzca una nueva contraseña</p>
            </div>

            <div class="col-12 mt-3">
                <form id="new_password-form">
                    <input type="hidden" name="uid" id="input-uid" value="{{ $uid }}" />
                    <div class="form-group">
                        <label for="input-password" class="text-dark-green font-weight-bold">Contraseña</label>
                        <input type="password" id="input-password" name="password" class="form-control" placeholder="Contraseña" />
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="form-group">
                        <label for="input-password2" class="text-dark-green font-weight-bold">Confirmar contraseña</label>
                        <input type="password" id="input-password2" name="password2" class="form-control" placeholder="Confirmar contraseña" />
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="form-group mt-4 text-center text-sm-left">
                        <button type="submit" class="p-0 bg-transparent border-0 w-100 w-sm-auto">
                            <img src="{{ config('app.url') }}/assets/images/continuar.png" alt="Acceder" class="img-fluid button" />
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcontent

@content('scripts')
    <script src="{{ config('app.url') }}/assets/js/new_password.js?{{ time() }}"></script>
@endcontent