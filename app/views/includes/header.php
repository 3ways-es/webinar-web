{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="header">
    <img src="{{ config('app.url') }}/assets/images/cabecera.png" alt="Header" class="img-fluid" />

    <?php if (auth()->loggedIn()): ?>
    <div class="row m-0 w-100 position-absolute top-0">
        <div class="col-3 offset-9">
            <a href="{{ route('logout')->path }}" title="Salir">
                <img src="{{ config('app.url') }}/assets/images/salir.png" alt="Salir" id="logout-button" class="img-fluid button position-absolute" />
            </a>
        </div>
    </div>
    <?php endif; ?>
</div>