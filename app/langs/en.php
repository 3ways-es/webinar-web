<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$_lang = [
    'first_key' => 'Value one', // __('first_key')
    'test' => [
        'key' => 'Hello' // __('test.key')
    ]
];
