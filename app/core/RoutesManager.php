<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Core;

class RoutesManager
{
    /**
     * Application routes variable
     *
     * @var App\Core\Route[]
     */
    private $routesList;

    /**
     * Current route
     *
     * @var App\Core\Route
     */
    public $route;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->routesList = [ ];
    }

    /**
     * Magic function to add route to the specific method
     *
     * @param string $function
     * @param array $arguments
     * 
     * @return mixed
     */
    public function __call($function, $arguments)
    {
        switch ($function):
            case 'get':
            case 'post':
            case 'delete':
            case 'put':
                // Add method to passed arguments
                array_splice($arguments, 1, 0, strtoupper($function));
                return $this->add(...$arguments);
            break;

            case 'api':
                // Change method and uri positions
                $uri = array_splice($arguments, 1, 1);
                array_splice($arguments, 0, 0, $uri);

                // Set argument $isApi to true
                array_splice($arguments, 3, 0, true);
                return $this->add(...$arguments);
            break;
        endswitch;
    }

    /**
     * Add route to list
     *
     * @param string $uri
     * @param string $method
     * @param object $invokable
     * @param object $isApi
     * 
     * @return App\Core\Route
     */
    public function add($uri, $method, $invokable, $isApi = false)
    {
        if (!strstr($uri, '/'))
            $uri = "/{$uri}";

        $uri = ($isApi ? "/api{$uri}" : $uri);
        $route = new Route($uri, strtolower($method), $invokable, $isApi);
        $uri_array = explode('/', $uri);
        for ($i = 0; $i < count($uri_array); $i++):
            $uri_param = $uri_array[$i];
            if (!strstr($uri_param, '{'))
                continue;

            $route->arguments[$uri_param] = "{$i}";
        endfor;
        
        $this->routesList[] = $route;
        return $route;
    }

    /**
     * Render the requested route
     *
     * @param string $method
     * @param string $uri
     * 
     * @return App\Core\View
     */
    public function render($method, $uri)
    {
        $this->parseUri($uri);
        
        $route = array_values(array_filter($this->routesList, function ($route) use ($method, $uri) {
            return $route->method == strtolower($method) && $route->path == $uri;
        }))[0] ?? null;
        
        if (!$route)
            foreach ($this->routesList as $current_route):
                if ($current_route->method != strtolower($method))
                    continue;

                $uri_array = explode('/', $uri);
                $current_main_uri = (($uri_array[1] == 'api') ? $uri_array[2] : $uri_array[1]);
                $route_array = explode('/', $current_route->path);
                $route_main_uri = (($route_array[1] == 'api') ? $route_array[2] : $route_array[1]);

                if (!strstr($route_main_uri, '{') && $current_main_uri != $route_main_uri)
                    continue;
                elseif (strstr($route_main_uri, '{') && empty($current_main_uri))
                    continue;
                elseif(count($uri_array) != count($route_array))
                    continue;

                $arguments = [ ];
                $virtual_route = $current_route->path;
                preg_match_all('/{(.*?)}/', $current_route->path, $matches);
                if (count($matches) > 0):
                    $matches = $matches[0];

                    for ($i = 0; $i < count($matches); $i++):
                        $parameter_index = ((int) $current_route->arguments[$matches[$i]]);
                        $virtual_route = str_replace($matches[$i], $uri_array[$parameter_index], $virtual_route);
                        $arguments[] = $route_array[$parameter_index];
                    endfor;
                endif;
                
                if ($virtual_route != $uri)
                    goto Error;
                
                $route = $current_route;
            endforeach;

        if (!$route)
            goto Error;
        
        $this->route = $route;
        return $route->render($uri);

        Error:
            return view('error');
    }

    /**
     * Get route by name
     *
     * @param string $name
     * 
     * @return App\Core\Route
     */
    public function getRouteByName($name)
    {
        return array_values(array_filter($this->routesList, function ($route) use ($name) {
            return $route->name == $name;
        }))[0] ?? null;
    }

    /**
     * Remove parameters from URI
     *
     * @param string $uri
     * 
     * @return string
     */
    public function parseUri(&$uri)
    {
        if (strpos($uri, '?') !== false):
            $uri_array = explode('?', $uri);
            $uri = $uri_array[0];
        endif;

        return $uri;
    }
}
