<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Core;

use App\Models\User;
use \hash;

class Authenticator
{
    /**
     * Authenticated user variable
     *
     * @var App\Models\User $user
     */
    private $user;

    /**
     * Try to login when class is initialized
     */
    public function __construct()
    {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])):
            $user_by_email = User::first('*', [ ['email', $_SESSION['email']] ]);
            if (!$user_by_email):
                $this->logout();
                return;
            endif;

            $hash = $this->checkHash($_SESSION['password'], $user_by_email);
            if (!$hash->isValid()):
                $this->logout();
                return;
            endif;

            $this->user = $user_by_email;
        elseif ($this->user):
            $this->logout();
        endif;
    }

    /**
     * Store current logged user
     *
     * @param App\Models\User $user
     * 
     * @return void
     */
    public function setUser($user, $password)
    {
        $this->user = $user;
        
        $_SESSION['email'] = $this->user->email;
        $_SESSION['password'] = $password;
    }

    /**
     * Get authenticated user instance
     *
     * @return App\Models\User|null
     */
    public function user()
    {
        return $this->user ?? null;
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function loggedIn()
    {
        return !is_null($this->user);
    }

    /**
     * Encrypt user password
     *
     * @param string $keyword
     * 
     * @return array
     */
    public function createHash($keyword)
    {
        $hash = new hash;
		$hash->password = $keyword;
		$hash->createHash();
        
        return $hash;
    }
    
    /**
     * Check password
     *
     * @param string $keyword
     * @param App\Models\User $user
     * 
     * @return array
     */
    public function checkHash($keyword, $user)
    {
        $hash = new hash;
        $hash->password = $keyword;
        $hash->salt = $user->password_s;
        $hash->hash = $user->password_h;
        
        return $hash;
    }

    /**
     * Close current user session
     *
     * @return void
     */
    public function logout()
    {
        $this->user = null;

        unset($_SESSION);
        session_destroy();
    }
}
