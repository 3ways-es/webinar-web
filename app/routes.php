<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$routes->get('/', 'IndexController@index')->name('index');
$routes->get('/preview', 'IndexController@preview')->name('preview');
$routes->get('/logout', 'AuthController@logout')->name('logout');
$routes->get('/seleccion', 'SeleccionController@index')->name('seleccion');
$routes->get('/streaming', 'StreamingController@index')->name('streaming');
$routes->get('/recuperar', 'RecoverPasswordController@index')->name('recover');
$routes->get('/new_password/{uid}/{key}', 'RecoverPasswordController@newPassword')->name('new_password');
$routes->get('/unsuscribe', 'UnsuscribeController@index')->name('unsuscribe');
$routes->get('/politica_cookies', function() {
    return view('policies.cookies');
})->name('cookies_policy');

$routes->get('/politica_privacidad', function() {
    return view('policies.privacy');
})->name('privacy_policy');

$routes->get('/condiciones_uso', function() {
    return view('policies.use');
})->name('conditions_of_use');

$routes->get('/actualizar_fuentes', function() {
    $file_path = __DIR__ . '/../storage/fuentes.html';
    $handle = fopen($file_path, 'w') or die("Cannot open file: {$file_path}");
    fwrite($handle, api_data(App\Models\ControlFuente::all()));
    fclose($handle);

    return 'Los datos fuentes se han actualizado correctamente en el archivo <b>storage/fuentes.html</b>';
});

// API Routes
$routes->api('post', '/auth/signup', 'AuthController@signup');
$routes->api('post', '/auth/signin', 'AuthController@signin');
$routes->api('post', '/users/recover', 'RecoverPasswordController@recover');
$routes->api('post', '/users/new_password', 'RecoverPasswordController@changePassword');
$routes->api('post', '/users/unsuscribe', 'UnsuscribeController@unsuscribe');
$routes->api('get', '/cookies/{name}', 'CookiesController@getCookie');
$routes->api('post', '/cookies', 'CookiesController@addCookie');
$routes->api('get', '/questions', 'QuestionsController@getQuestions');
$routes->api('post', '/questions', 'QuestionsController@createQuestion');
$routes->api('put', '/questions/{id}', 'QuestionsController@editQuestion');
$routes->api('delete', '/questions/{id}', 'QuestionsController@deleteQuestion');
$routes->api('post', '/users_tiempos', 'StreamingController@registerUserTiempo');
$routes->api('put', '/users_tiempos/{payload}', 'StreamingController@unregisterUserTiempo');
$routes->api('get', '/control_fuentes/{id}', 'StreamingController@getControlFuente');
