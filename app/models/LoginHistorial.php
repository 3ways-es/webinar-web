<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Models;

class LoginHistorial extends Model
{
    protected $table = 'login_historial';
}
