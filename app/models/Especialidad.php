<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Models;

class Especialidad extends Model
{
    protected $table = 'especialidades';
}
