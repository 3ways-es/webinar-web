# Webinar Web
-----------

1. Renombrar archivo de configuración `config.ini.example` a `config.ini` y realizar los cambios correspondientes en el archivo
2. Ejecutar el siguiente comando en la base de datos
```sql
    ALTER TABLE `users` ADD `nacionalidad` VARCHAR(255) NULL AFTER `estado`, ADD `nif` VARCHAR(255) NULL AFTER `nacionalidad`, ADD COLUMN `token` VARCHAR(255) NULL;
    ALTER TABLE `preguntas` ADD COLUMN `corregido` VARCHAR(4500) NULL, ADD COLUMN `hora_proyectar` TIMESTAMP NULL;
    ALTER TABLE `controlfuentes` ADD COLUMN `chat` TINYINT(1) NULL, ADD COLUMN `chat_source` TEXT NULL;
    INSERT INTO `users`(`idusuario`, `usuario`, `clave`, `tipo`) VALUES(3, 'editor', 'editor', 3);
	INSERT INTO `controlfuentes`(`idControlFuente`, `descripcion`, `source`, `activo`, `updateFrequency`, `audio`, `chat`, `chat_source`) VALUES('6', 'Eliminar interval', NULL, '0', NULL, NULL, NULL, NULL);
```

# Webinar Sockets (webinar-sockets)
------------

1. Renombrar archivo de configuración `config.ini.example` a `config.ini` y realizar los cambios correspondientes en el archivo
2. Iniciar el servidor ejecutando el comando `php server.php`

# Preguntas Webinar (preguntas)
------------
1. Renombrar archivo de configuración `config.ini.example` a `config.ini`

2. Dentro del archivo de configuración habría que configurar `app.url` y `app.subdir` de la siguiente manera:
	- **app.url** no debe tener `/` al final
	- **app.subdir** si el proyecto está en un subdirectorio, por ejemplo, si la URL es `http://webinar.com/subdirectorio/` el valor que habría que darle sería `/subdirectorio`, sin `/` al final. En caso de que la URL sea solo `http://webinar.com/` habría que dejarlo vacío.

> Si el servidor donde se ha alojado el proyecto **no es Windows** y **no está en un subdirectorio**, el proyecto ya estaría funcionando correctamente.

5. Si el servidor es Windows, y está en un subdirectorio habría que añadir lo siguiente en el `web.config` de la **raíz**:
	- Dentro de la etiqueta `<rules></rules>` agregar el siguiente código y cambiar **SUBDIRECTORIO** por el nombre del subdirectorio
```xml
		<rule name="Preguntas Rule">
			<match url="^SUBDIRECTORIO/(.*)$" ignoreCase="false" />
			<conditions>
				<add input="{REQUEST_FILENAME}" matchType="IsDirectory" ignoreCase="false" negate="true" />
				<add input="{REQUEST_FILENAME}" matchType="IsFile" ignoreCase="false" negate="true" />
			</conditions>
			<action type="Rewrite" url="SUBDIRECTORIO/index.php" appendQueryString="true" />
		</rule>
```
	- Para que se acepten los métodos PUT y DELETE que pueden ser usados en las rutas, habría añadir las siguientes líneas, dentro de la etiqueta `<system.webServer></system.webServer>`
		1. Agregar todos los métodos válidos
```xml
			<security>
				<requestFiltering>
					<verbs allowUnlisted="false">
						<add verb="GET" allowed="true" />
						<add verb="POST" allowed="true" />
						<add verb="DELETE" allowed="true" />
						<add verb="PUT" allowed="true" />
					</verbs>
				</requestFiltering>
			</security>
```
		
		2. Deshabilitar WebDAV, se hace dentro de la etiqueta `<handlers></handlers>`
```xml
			<remove name="WebDAV" />
```

		3.  Deshabilitar WebDAVModule
```xml
			<modules>
				<remove name="WebDAVModule" />
			</modules>
```

# TO-DO List
------------
[ ] Integrar preguntas con el proyecto principal
[ ] Integrar webinar-sockets con el proyecto principal