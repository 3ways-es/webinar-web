<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Core;

use App\Models\Staff;

class Authenticator
{
    /**
     * Authenticated user variable
     *
     * @var App\Models\Staff $user
     */
    private $user;

    /**
     * Try to login when class is initialized
     */
    public function __construct()
    {   
        if (config('app.auth_by_token') && isset($_SESSION['app_token'])):
            $this->user = Staff::getUserByToken($_SESSION['app_token']);
        elseif (!config('app.auth_by_token') && (isset($_SESSION['app_username']) && isset($_SESSION['app_password']))):
            $this->user = Staff::first('*', [
                ['usuario', $_SESSION['app_username']],
                ['clave', $_SESSION['app_password']]
            ]);
        elseif ($this->user):
            $this->logOut();
        endif;
    }

    /**
     * Store current logged user
     *
     * @param \App\Models\Staff $user
     * 
     * @return void
     */
    public function setUser(Staff $user)
    {
        $this->user = $user;

        if (config('app.auth_by_token')):
            $_SESSION['app_token'] = $this->user->token;
        else:
            $_SESSION['app_username'] = $this->user->usuario;
            $_SESSION['app_password'] = $this->user->clave;
        endif;
    }

    /**
     * Get authenticated user instance
     *
     * @return App\Models\Staff|null
     */
    public function user()
    {
        return $this->user ?? null;
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function loggedIn()
    {
        return !is_null($this->user);
    }

    /**
     * Encrypt user password
     *
     * @param string $keyword
     * 
     * @return string
     */
    public function encrypt($keyword)
    {
		$encrypted = md5($keyword);
        $encrypted = hash('gost', $encrypted);
        $encrypted = hash('whirlpool', $encrypted);
        $encrypted = hash('sha512', $encrypted);
        
        return $encrypted;
    }

    /**
     * Close current user session
     *
     * @return void
     */
    public function logOut()
    {
        $this->user = null;

        unset($_SESSION);
        session_destroy();
    }
}
