<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Core;

use App\Controllers\Controller;
use stdClass;

class Routes
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    private $routes;

    /**
     * Undocumented function
     * 
     * @return void
     */
    public function __construct()
    {
        $this->routes = new stdClass;

        // Default error message
        $this->routes->get['error'] = (function() {
            return "Cannot {$_SERVER['REQUEST_METHOD']} {$_SERVER['REQUEST_URI']}";
        })();
    }

    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $uri
     * 
     * @return void
     */
    public function render($method, $uri)
    {
        $this->parseUri($uri);
        
        $method = strtolower($method);
        $arguments = [ ];
        $virtual_route = '';

        if (!array_key_exists($uri, $this->routes->{$method})):
            $uri_array = explode('/', $uri);
            $current_main_uri = $uri_array[1];

            foreach ($this->routes->{$method} as $route => $values):
                if ($route == 'error') continue;
                
                $route_array = explode('/', $route);
                $route_main_uri = $route_array[1];
                if (!strstr($route_main_uri, '{') && $current_main_uri != $route_main_uri) continue;
                elseif (strstr($route_main_uri, '{') && empty($current_main_uri)) continue;
                elseif(count($uri_array) != count($route_array)) continue;

                $virtual_route = $route;
                preg_match_all('/{(.*?)}/', $route, $matches);
                if (count($matches) > 0):
                    $matches = $matches[0];
                    for ($i = 0; $i < count($matches); $i++):
                        $parameter_index = (int) $values['parameters'][$matches[$i]];
                        $virtual_route = str_replace($matches[$i], $uri_array[$parameter_index], $virtual_route);
                        $arguments[] = $route_array[$parameter_index];
                    endfor;
                endif;
                
                if ($virtual_route != $uri || !array_key_exists($route, $this->routes->{$method}))
                    return $this->routes->get['error'];
                
                $uri = $route;
            endforeach;
        endif;
        
        $route = $this->routes->{$method}[$uri] ?? null;
        if (is_null($route)) return $this->routes->get['error'];

        $function = $route['invokable'];
        if (gettype($function) !== 'object'):
            $function_array = explode('@', $function);
            $controller_name = "App\\Controllers\\{$function_array[0]}";
            $stdClass = new $controller_name;

            $parameters = get_function_parameters($function_array[1], $stdClass);
            $arguments = [ ];
            $uri_array = explode('/', $uri);
            for ($i = 0; $i < count($uri_array); $i++):
                $uri_param = $uri_array[$i];
                if (!strstr($uri_param, '{')) continue;
                $current_uri = explode('/', $virtual_route);
                $uri = str_replace($uri_param, $current_uri[$i], $uri);
                $arguments[] = $current_uri[$i];
            endfor;
            
            if (count($parameters) > count($arguments)) $arguments[] = $this->getRequest($method);
            
            if (array_key_exists('api', $route) && $route['api'] === true):
                header('Content-type: application/json');
                return api_data(call_user_func_array([ $stdClass, $function_array[1] ], $arguments)) ?? $this->routes->get['error'];
            endif;

            return call_user_func_array([ $stdClass, $function_array[1] ], $arguments) ?? $this->routes->get['error'];
        else:
            $parameters = get_function_parameters($function);
            $arguments = [ ];
            $uri_array = explode('/', $uri);
            for ($i = 0; $i < count($uri_array); $i++):
                $uri_param = $uri_array[$i];
                if (!strstr($uri_param, '{')) continue;
                $current_uri = explode('/', $virtual_route);
                $uri = str_replace($uri_param, $current_uri[$i], $uri);
                $arguments[] = $current_uri[$i];
            endfor;

            if (count($parameters) > count($arguments)) $arguments[] = $this->getRequest($method);

            if (array_key_exists('api', $route) && $route['api'] === true):
                header('Content-type: application/json');
                return api_data(call_user_func_array($function, $arguments)) ?? $this->routes->get['error'];
            endif;

            return call_user_func_array($function, $arguments) ?? $this->routes->get['error'];
        endif;

        return $this->routes->get['error'];
    }

    /**
     * Undocumented function
     *
     * @param void $function
     * 
     * @return void
     */
    public function error($function)
    {
        $this->routes->get['error'] = $function();
    }

    /**
     * Undocumented function
     *
     * @param string $uri
     * @param void $function
     * 
     * @return void
     */
    public function get($uri, $function)
    {
        if (!$this->addMultipleUris('get', $uri, $function))
            $this->addRoute('get', $uri, $function);
    }
    
    /**
     * Undocumented function
     *
     * @param string $uri
     * @param void $function
     * 
     * @return void
     */
    public function post($uri, $function)
    {
        if (!$this->addMultipleUris('post', $uri, $function))
            $this->addRoute('post', $uri, $function);
    }
    
    /**
     * Undocumented function
     *
     * @param string $uri
     * @param void $function
     * 
     * @return void
     */
    public function put($uri, $function)
    {
        if (!$this->addMultipleUris('put', $uri, $function))
            $this->addRoute('put', $uri, $function);
    }

    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $uri
     * @param void $function
     * 
     * @return void
     */
    public function api($method, $uri, $function)
    {
        $this->addRoute(strtolower($method), "/api/{$uri}", $function, true);
    }

    /**
     * Add multiple uris to the same function
     *
     * @param string $method
     * @param array $uris
     * @param object $function
     * 
     * @return bool
     */
    private function addMultipleUris($method, $uris, $function)
    {
        if (!is_array($uris)) return false;

        foreach ($uris as $uri) $this->addRoute($method, $uri, $function);
        return true;
    }

    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $uri
     * @param void $function
     * @param bool $api
     * 
     * @return void
     */
    private function addRoute($method, $uri, $function, $api = false)
    {
        if (!strstr($uri, '/')) $uri = "/{$uri}";

        $this->routes->{$method}[$uri]['invokable'] = $function;
        if ($api) $this->routes->{$method}[$uri]['api'] = true;

        $arguments = [ ];
        $uri_array = explode('/', $uri);
        for ($i = 0; $i < count($uri_array); $i++):
            $uri_param = $uri_array[$i];
            if (!strstr($uri_param, '{')) continue;
            $this->routes->{$method}[$uri]['parameters'][$uri_param] = "{$i}";
        endfor;
    }

    /**
     * Undocumented function
     *
     * @param string $method Request method
     * 
     * @return stdClass
     */
    public function getRequest($method)
    {
        $request = new stdClass;

        switch (strtoupper($method)):
            case 'POST':
                $parameters = json_decode(file_get_contents('php://input'), true);
                if (is_array($parameters)) $_POST = array_merge($parameters, $_POST);

                $request->input = json_decode(json_encode($_POST));
                $request->files = $_FILES;
                $request->query = json_decode(json_encode($_GET));
            break;

            case 'GET':
                $request->input = json_decode(json_encode($_GET));
                $request->files = $_FILES;
            break;

            case 'DELETE':
            case 'PUT':
                $parameters = json_decode(file_get_contents('php://input'), true);
                if (is_array($parameters)) $_POST = array_merge($parameters, $_POST);
                
                $request->input = json_decode(json_encode($_POST));
                $request->files = $_FILES;
                $request->query = json_decode(json_encode($_GET));
            break;
        endswitch;

        $request->path = $_SERVER['REQUEST_URI'];

        return $request;
    }

    /**
     * Undocumented function
     *
     * @param string $uri Current server URI
     * 
     * @return void
     */
    public function parseUri(&$uri)
    {
        if (strpos($uri, '?') !== false):
            $uri_array = explode('?', $uri);
            $uri = $uri_array[0];
        endif;

        return $uri;
    }
}
