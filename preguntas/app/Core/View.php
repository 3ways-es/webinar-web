<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Core;

class View
{
    /**
     * Build the requested view
     * 
     * @return string
     */
    public static function make($view, $params = [ ])
    {
        self::build($view, $viewData);
        $viewData = preg_replace("/@get[(]'(.*?)'[)]/", '', $viewData);
        $viewData = preg_replace('/%-(.*?)-%/', '', $viewData);

        // Set parameters
        foreach ($params as $param => $value)
            ${$param} = $value;

        // Parse variables and functions
        preg_match_all('/%% (.*?) %%/', $viewData, $matches);
        foreach ($matches[1] as $match)
            $viewData = str_replace("%% {$match} %%", @eval("return {$match};"), $viewData);

        return self::minify($viewData);
    }

    /**
     * Undocumented function
     * 
     * @return void
     */
    private static function build($view, &$viewData)
    {
        self::parsePath($view);
        self::loadContent($view, $viewData);
        self::addIncludes($viewData);
        self::getContent($viewData);
    }

    /**
     * Parse view path
     * 
     * @return string
     */
    private static function parsePath(&$view)
    {
        $view = str_replace('.', '/', $view);
        return $view;
    }

    /**
     * Get view content from file
     * 
     * @return string
     */
    private static function loadContent($view, &$viewData)
    {
        $file_path = "{$_SERVER['DOCUMENT_ROOT']}/views/{$view}.php";
        if (!file_exists($file_path)):
            $viewData = "Cannot load view '{$view}'";
        else:
            ob_start();

            include($file_path);
            $viewData = ob_get_clean();
        endif;

        return $viewData;
    }

    /**
     * Undocumented function
     * 
     * @param string $viewData View content
     * 
     * @return string
     */
    private static function addIncludes(&$viewData)
    {
        preg_match_all("/@add[(]'(.*?)'[)]/", $viewData, $matches);
        foreach ($matches[1] as $match):
            $view_name = $match;
            self::build($view_name, $content);

            $viewData = str_replace("@add('{$match}')", $content, $viewData);
        endforeach;

        return $viewData;
    }

    /**
     * Undocumented function
     * 
     * @param string $viewData View content
     * 
     * @return string
     */
    private static function getContent(&$viewData)
    {
        preg_match_all("/@content[(]'(.*?)'[)](.*?)@endcontent/s", $viewData, $contents);
        for ($i = 0; $i < count($contents[0]); $i++):
            $viewData = preg_replace("/@get[(]'{$contents[1][$i]}'[)]/", $contents[2][$i], $viewData);
            $viewData = preg_replace("/@content[(]'{$contents[1][$i]}'[)](.*?)@endcontent/s", '', $viewData);
        endfor;
        
        return $viewData;
    }

    /**
     * Undocumented function
     * 
     * @param string $viewData View content
     * 
     * @return string
     */
    private static function minify(&$viewData)
    {
        // Minify HTML
        $replacements = [ '>', '<', '\\1', '' ];
        $expressions = [
            '/\>[^\S]+/s',
            '/[^\S]+\</s',
            '/(\s)+/s',
            '/<!--(.|\s)*?-->/'
        ];
        
        $viewData = preg_replace($expressions, $replacements, $viewData);

        // Minify Styles
        preg_match_all('/<style(.*?)>(.*?)<\/style>/s', $viewData, $styles);
        foreach ($styles[2] as $style):
            $minified = $style;
            $minified = preg_replace('/\/\*((?!\*\/).)*\*\//', '', $minified);
            $minified = preg_replace('/\s{2,}/', ' ', $minified);
            $minified = preg_replace('/\s*([:;{}])\s*/', '$1', $minified);
            $minified = preg_replace('/;}/', '}', $minified);

            $viewData = str_replace($style, $minified, $viewData);
        endforeach;

        // Minify scripts
        preg_match_all('/<script(.*?)>(.*?)<\/script>/s', $viewData, $scripts);
        foreach ($scripts[2] as $script):
            $minified = $script;
            $minified = preg_replace('#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#', '$1', $minified);
            $minified = preg_replace('#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s', '$1$2', $minified);
            $minified = preg_replace('#;+\}#', '}', $minified);
            $minified = preg_replace('#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i', '$1$3', $minified);
            $minified = preg_replace('#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i', '$1.$3', $minified);

            $viewData = str_replace($script, $minified, $viewData);
        endforeach;
        
        return $viewData;
    }
}
