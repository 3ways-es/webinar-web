<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class Message extends Model
{
    protected $table = 'mensajes_v2';
    protected $hidden = [ 'id_usuario_destino' ];
    protected $functionsToShow = [ 'sender' ];

    /**
     * Get message sender
     *
     * @return \App\Models\Staff
     */
    public function sender()
    {
        return $this->belongsTo(Staff::class, 'id_usuario_origen');
    }
}
