<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class Especialidad extends Model
{
    protected $table = 'especialidades';
}
