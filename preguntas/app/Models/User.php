<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class User extends Model
{
    protected $hidden = [ 'password_s', 'password_h', 'token', 'especialidad', 'id_provincia' ];
    protected $functionsToShow = [ 'especialidad', 'provincia' ];

    /**
     * Get user especialidad
     *
     * @return \App\Models\Especialidad
     */
    public function especialidad()
    {
        return $this->belongsTo(Especialidad::class, 'especialidad')->especialidad;
    }
    
    /**
     * Get user provincia
     *
     * @return \App\Models\Provincia
     */
    public function provincia()
    {
        return $this->belongsTo(Provincia::class, 'id_provincia')->provincia;
    }
}
