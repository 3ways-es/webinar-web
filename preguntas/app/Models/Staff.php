<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class Staff extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'idusuario';
    protected $hidden = [ 'clave', 'token' ];

    /**
     * Get user messages
     *
     * @return App\Models\Message
     */
    public function messages()
    {
        return Message::select('*', [ ['id_usuario_origen', $this->idusuario, [ 'id_usuario_destino', $this->idusuario ]] ], [ ['id', 'DESC'] ]);
    }

    /**
     * Get user instance by token
     *
     * @return App\Models\User
     */
    public static function getUserByToken($token)
    {
        return Staff::first('*', [ ['token', $token] ]);
    }
}
