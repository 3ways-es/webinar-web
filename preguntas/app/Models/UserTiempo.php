<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class UserTiempo extends Model
{
    protected $table = 'users_tiempos';
    protected $hidden = [ 'talleres_id' ];
}
