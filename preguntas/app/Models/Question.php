<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Models;

class Question extends Model
{
    protected $table = 'preguntas';
    protected $primaryKey = 'idpregunta';
    protected $hidden = [ 'administrador_idusuario', 'administrador_fecha', 'moderador_idusuario', 'moderador_fecha' ];
}
