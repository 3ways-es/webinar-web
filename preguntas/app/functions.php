<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

/**
 * Initialize database connection
 * 
 * @var App\Core\Database $database
 */
$_database = null;

/**
 * Authenticator instance
 * 
 * @var App\Core\Authenticator $authenticator
 */
$_authenticator = null;

/**
 * Configuration values array
 * 
 * @var array $_config
 */
$_config = null;

/**
 * Initialize routes class
 * 
 * @var App\Core\Routes $routes
 */
$routes = null;

/**
 * Load all application components
 */
function initialize_app()
{
   global $routes, $_config, $_database, $_authenticator;

   // Load all app core files
   foreach (glob($_SERVER['DOCUMENT_ROOT'] . '/app/Core/*.php') as $core_file)
      require_once $core_file;

   $_config = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/config.ini');
   $routes = new App\Core\Routes;

   // Load all app routes, models and controllers
   require_once $_SERVER['DOCUMENT_ROOT'] . '/app/routes.php';
   require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Models/Model.php';
   require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Controllers/Controller.php';

   foreach (glob($_SERVER['DOCUMENT_ROOT'] . '/app/{Models,Controllers}/*.php', GLOB_BRACE) as $app_file):
      $file_name = pathinfo($app_file, PATHINFO_FILENAME);
      if ($file_name === 'Model' || $file_name === 'Controller') continue;
      
      require_once $app_file;
   endforeach;
   
   $_database = new App\Core\Database;
   $_authenticator = new App\Core\Authenticator;
}

/**
* Build view
* 
* @param string $view View name
* @param array $params View parameters
* 
* @return App\Core\View
*/
function view($view, $params = [ ])
{
   return App\Core\View::make($view, $params);
}

/**
* Get configuration value
* 
* @param string $key Configuration variable name
* 
* @return object
*/
function config($key)
{
   global $_config;

   return $_config[$key];
}

/**
 * Get database instance
 * 
 * @return App\Core\Database
 */
function database()
{
    global $_database;

    return $_database;
}

// /**
//  * Get route path by name
//  *
//  * @param string $name Route name
//  * 
//  * @return string
//  */
// function route($name)
// {
//      TO-DO function    
// }

/**
* Undocumented function
*
* @param object $function Function
* @param mixed $class Class
* 
* @return array
*/
function get_function_parameters($function, $class = null)
{
   $result = [ ];
   $reflection = (!is_null($class) ? new ReflectionMethod($class, $function) : new ReflectionFunction($function));
   foreach ($reflection->getParameters() as $param)
       $result[] = $param->name;
   
   return $result;
}

/**
 * Undocummented function
 * 
 * @var mixed $data Data to parse
 * 
 * @return string
 */
function api_data($data)
{
   parse_data($data);
   return (!is_array($data) ? $data : json_encode($data));
}

function parse_data(&$data)
{
   if (gettype($data) === 'array'):
      $data = array_map(function ($element) {
         if (gettype($element) === 'object' && !is_null(get_class($element)))
            return $element->__toArray();
         else
            return parse_data($element);
      }, $data);
   elseif (gettype($data) === 'object' && !is_null(get_class($data))):
      $data = $data->__toArray();
   endif;

   return $data;
}

/**
 * Get authenticator instance
 * 
 * @return App\Core\Authenticator
 */
function auth()
{
   global $_authenticator;

   return $_authenticator;
}

/**
 * Get excluded mail domains
 *
 * @return array
 */
function excluded_mail_domains()
{
   if (empty(config('app.excluded_mail_domains')))
      return '/';

   $excluded_domains = explode(',', config('app.excluded_mail_domains'));
   $excluded_domains = array_map(function($excluded_domain) { return "@{$excluded_domain}"; }, $excluded_domains);

   return implode('|', $excluded_domains);
}