<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

global $routes;

// Site Routes
$routes->get([ '/', '/questions', '/analytics' ], 'IndexController@render');
$routes->get('/proyeccion', function() {
    return view('screening');
});

// API Routes
$routes->api('post', 'auth/login', 'AuthenticatorController@tryLogin');
$routes->api('post', 'auth/logout', 'AuthenticatorController@logOut');
$routes->api('get', 'auth/data', 'AuthenticatorController@getAuthUser');
$routes->api('get', 'user/messages', 'MessagesController@getReceivedMessages');
$routes->api('get', 'questions', 'QuestionsController@getQuestions');
$routes->api('post', 'messages/send', 'MessagesController@sendMessage');
$routes->api('get', 'questions/current_screening', 'QuestionsController@getCurrentScreening');
$routes->api('put', 'questions/{id}/update/status', 'QuestionsController@updateStatus');
$routes->api('put', 'questions/{id}/update', 'QuestionsController@updateQuestion');
$routes->api('get', 'analytics', 'AnalyticsController@getAnalytics');
$routes->api('get', 'analytics/export/{type}', 'AnalyticsController@export');