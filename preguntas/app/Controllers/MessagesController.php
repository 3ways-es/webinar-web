<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

use App\Models\Message;

class MessagesController extends Controller
{
    /**
     * Get all messages from current logged user
     *
     * @return \App\Models\Message
     */
    public function getReceivedMessages()
    {
        $this->permissions([ 'auth' ], true);
        
        if (auth()->user()->tipo != 1 && auth()->user()->tipo != 2) return null;

        return auth()->user()->messages();
    }

    /**
     * Send message
     * 
     * @return \App\Models\Message
     */
    public function sendMessage($request)
    {
        $this->permissions([ 'auth' ], true);

        if (auth()->user()->tipo != 2 || !$request->input->message) return null;

        $message = new Message;
        $message->id_usuario_origen = auth()->user()->idusuario;
        $message->id_usuario_destino = 1;
        $message->texto = $request->input->message;
        $message->fecha = date('Y-m-d H:i:s', time());

        $message->save();

        return $message;
    }
}
