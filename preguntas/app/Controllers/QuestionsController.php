<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

use App\Models\Question;

class QuestionsController extends Controller
{
    /**
     * Get all messages from current logged user
     *
     * @return \App\Models\Question[]
     */
    public function getQuestions()
    {
        $this->permissions([ 'auth' ], true);
        
        if (auth()->user()->tipo != 2)
            return Question::all([ ['estado', '!=', 0] ], [ ['orden', 'DESC'] ]);
            
        return Question::all([ ['idpregunta', 'DESC'] ]);
    }

    /**
     * Get current screening question
     *
     * @return \App\Models\Question
     */
    public function getCurrentScreening()
    {
        $question = Question::first('pregunta, corregido, hora_proyectar', [ ['hora_proyectar', '!=', 'null'] ], [ ['hora_proyectar', 'DESC'] ]);

        if($question->corregido){
          $pregunta = [$question->corregido, $question->hora_proyectar];
          return (json_encode($pregunta));
        }
        $pregunta = [$question->pregunta, $question->hora_proyectar];
        return (json_encode($pregunta));
    }

    /**
     * Update question status by id
     *
     * @param int $id Question id
     * 
     * @return \App\Models\Question
     */
    public function updateStatus($id)
    {
        $this->permissions([ 'auth' ], true);

        $question = Question::find($id);
        if (!$question) return null;

        switch (auth()->user()->tipo):
            case 3: // Editor
                $question->hora_proyectar = date('Y-m-d H:i:s', time());
            break;

            case 2: // Administrator
                // $question_ordered = Question::first('orden', [ ['orden', '>', 0] ], [ ['orden', 'DESC'] ]);
                // $question->orden = ($question_ordered ? ($question_ordered->orden + 1) : 1);
                $question->estado = (($question->estado == 0) ? 1 : 0);
            break;

            case 1: // Moderator
                $question->estado = (($question->estado == 1) ? 2 : 1);
            break;
        endswitch;

        $question->save();
        return $question;
    }

    /**
     * Update question by id
     *
     * @param int $id Question id
     * @param array $request
     * 
     * @return \App\Models\Question
     */
    public function updateQuestion($id, $request)
    {
        $this->permissions([ 'auth' ], true);
        if (auth()->user()->tipo != 3) return null;

        $question = Question::find($id);
        if (!$question) return null;

        $question->corregido = $request->input->message;

        $question->save();
        return $question;
    }
}
