<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

use App\Models\Staff;

class AuthenticatorController extends Controller
{
    /**
     * Try to login by username and password
     * generate a session token and set user in
     * \App\Core\Authenticator instance
     *
     * @param object $request
     * 
     * @return array
     */
    public function tryLogin($request)
    {
        $this->permissions([ 'guest' ], true);

        $data = [ ];

        if (!$request->input->username) $data['errors']['username'][] = 'El nombre de usuario no puede estar vacio';
        if (!$request->input->password) $data['errors']['password'][] = 'La contraseña no puede estar vacia';

        // Data validation process
        $username = Staff::first('idusuario', [ ['usuario', $request->input->username] ]);
        if (!$username):
            $data['errors']['username'][] = 'El nombre de usuario no existe';
            goto Result;
        endif;

        $user = Staff::first('*', [ ['usuario', $request->input->username], ['clave', $request->input->password] ]);
        if (!$user):
            $data['errors']['password'][] = 'La contraseña no es correcta';
            goto Result;
        endif;

        if (config('app.auth_by_token')) $user->token = auth()->encrypt(md5(time()) . sha1(rand(0, time()) . $user->clave));
        $user->save();
        
        $data['user'] = $user;
        if (config('app.auth_by_token')) $data['token'] = $user->token;
        
        auth()->setUser($user);
        
        Result:
        return $data;
    }

    /**
     * LogOut from current user
     *
     * @return void
     */
    public function logOut()
    {
        auth()->logOut();
    }

    /**
     * Get logged user data
     * 
     * @return \App\Models\Staff
     */
    public function getAuthUser()
    {
        return auth()->user();
    }
}
