<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

use App\Models\Question;
use App\Models\User;
use App\Models\UserTiempo;
use DatePeriod;
use DateTime;
use DateInterval;

class AnalyticsController extends Controller
{
    /**
     * Get all streaming data
     *
     * @return array
     */
    public function getAnalytics()
    {
        $this->permissions([ 'auth' ], true);
        if (auth()->user()->tipo != 3) return null;

        $current_range = '';
        $streaming_start = strtotime(config('streaming.date') . ' ' . config('streaming.start'));
        $streaming_end = strtotime(config('streaming.date') . ' ' . config('streaming.end'));
        if ($streaming_start > time()):
            $current_range = 'Sin empezar';
        elseif ($streaming_end < time()):
            $current_range = 'Finalizado';
        else:
            $range = $this->getCurrentRange();
            $current_range = (($range->from ?? '00:00') . ' - ' . ($range->to ?? '00:00'));
        endif;
        
        $online_users = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada >= \'' . date('Y-m-d H:i', strtotime('-30 minute', $streaming_start)) . '\' AND salida IS NULL AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
        $total_users = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada >= \'' . date('Y-m-d H:i', strtotime('-30 minute', $streaming_start)) . '\' AND (salida <= \'' . date('Y-m-d H:i', strtotime('+2 hour', $streaming_end)) . '\' OR salida IS NULL) AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');

        return [
            'date' => config('streaming.date'),
            'start' => config('streaming.start'),
            'end' => config('streaming.end'),
            'currentRange' => $current_range,
            'onlineUsers' => $online_users,
            'totalUsers' => $total_users,
            'questionCount' => Question::count([ ['idusuariogesida', 'IN', '(SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\')', ['idusuariogesida', 'IS', 'NULL']] ]),
            'userCount' => User::count([ ['email', 'NOT REGEXP', excluded_mail_domains()] ]),
            'newUsers' => User::count([ ['date_add', '>=', date('Y-m-d H:i:s', strtotime(config('app.release_date')))], ['email', 'NOT REGEXP', excluded_mail_domains()] ]),
            'allowNewsletters' => User::count([ ['acepto_publicar', 1], ['email', 'NOT REGEXP', excluded_mail_domains()] ]),
            'screenedQuestions' => Question::count([ ['hora_proyectar', '!=', 'null'], ['idusuariogesida', 'IN', '(SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\')', ['idusuariogesida', 'IS', 'NULL']] ]),
            'answeredQuestions' => Question::count([ ['estado', 2], ['idusuariogesida', 'IN', '(SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\')', ['idusuariogesida', 'IS', 'NULL']] ]),
            'editedQuestions' => Question::count([ ['corregido', '!=', 'null'], ['idusuariogesida', 'IN', '(SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\')', ['idusuariogesida', 'IS', 'NULL']] ])
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $type
     * @return void
     */
    public function export($type)
    {
        $this->permissions([ 'auth' ], true);
        if (auth()->user()->tipo != 3) return null;

        $columns = null;
        $data = null;

        switch ($type):
            case 'questions':
                $columns = Question::getColumnNames();
                $data = Question::all([ ['idusuariogesida', 'IN', '(SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\')', ['idusuariogesida', 'IS', 'NULL']] ], null);
            break;

            case 'users':
                $columns = User::getColumnNames();
                $data = User::all([ ['email', 'NOT REGEXP', excluded_mail_domains()] ], null);
            break;

            case 'new_users':
                $columns = User::getColumnNames();
                $data = User::all([ ['date_add', '>=', date('Y-m-d H:i', strtotime(config('app.release_date')))], ['email', 'NOT REGEXP', excluded_mail_domains()] ], null);
            break;
            
            case 'users_per_range':
                $date_start = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.start')));
                $date_end = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.end')));
                
                $ranges = $this->getTimeRanges();
                $columns = [ 'rango', 'nombre', 'apellidos', 'email', 'especialidad', 'provincia', 'codigo_postal', 'poblacion', 'centro' ];
                
                $all_data = [];

                foreach ($ranges as $key => $range):
                    $date_from = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['from'];
                    $date_to = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['to'];
                    $current_data = null;
                    $current_range_text = ('De ' . $range['from'] . ' a ' . $range['to']);

                    if ($key == 0): // First range
                        $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_from . '\' AND entrada < \'' . $date_to . '\' AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

                        foreach ($results as $result):
                            $result['rango'] = $current_range_text;
                            $current_data[] = $result;
                        endforeach;
                    elseif ($key == (count($ranges) - 1)): // Last range
                        $previous_range = $ranges[$key - 1];
                        
                        $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_start . '\' AND entrada < \'' . $date_end . '\' AND (salida > \'' . $previous_range['from'] . '\' OR salida IS NULL) AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

                        foreach ($results as $result):
                            $result['rango'] = $current_range_text;
                            $current_data[] = $result;
                        endforeach;
                    else:
                        $previous_range = $ranges[$key - 1];

                        $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_start . '\' AND entrada < \'' . $date_to . '\' AND ((salida < \'' . $previous_range['to']. '\' AND salida > \'' . $previous_range['from'] . '\') OR salida > \'' . $previous_range['to'] . '\' OR salida IS NULL) AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

                        foreach ($results as $result):
                            $result['rango'] = $current_range_text;
                            $current_data[] = $result;
                        endforeach;
                    endif;
                    
                    $all_data = array_merge($all_data, $current_data);
                endforeach;

                $data = $all_data;
            break;
            
            case 'count_per_range':
                $date_start = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.start')));
                $date_end = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.end')));

                $ranges = $this->getTimeRanges();
                $columns = [ 'rango', 'conectados' ];

                foreach ($ranges as $key => $range):
                    $date_from = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['from'];
                    $date_to = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['to'];

                    $current_data = [
                        'rango' => ('De ' . $range['from'] . ' a ' . $range['to']),
                        'conectados' => 0
                    ];
                    
                    if ($key == 0): // First range
                        $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_from . '\' AND entrada < \'' . $date_to . '\' AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
                    elseif ($key == (count($ranges) - 1)): // Last range
                        $previous_range = $ranges[$key - 1];
                        
                        $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_start . '\' AND entrada < \'' . $date_end . '\' AND (salida > \'' . $previous_range['from'] . '\' OR salida IS NULL) AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
                    else:
                        $previous_range = $ranges[$key - 1];

                        $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_start . '\' AND entrada < \'' . $date_to . '\' AND ((salida < \'' . $previous_range['to']. '\' AND salida > \'' . $previous_range['from'] . '\') OR salida > \'' . $previous_range['to'] . '\' OR salida IS NULL) AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
                    endif;
                    
                    $data[] = $current_data;
                endforeach;
            break;
            
            case 'total_online_users':
                $streaming_start = strtotime(config('streaming.date') . ' ' . config('streaming.start'));
                $streaming_end = strtotime(config('streaming.date') . ' ' . config('streaming.end'));
                
                $columns = [ 'nombre', 'apellidos', 'email', 'especialidad', 'provincia', 'codigo_postal', 'poblacion', 'centro' ];
                $data = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada >= \'' . date('Y-m-d H:i', strtotime('-30 minute', $streaming_start)) . '\' AND (salida <= \'' . date('Y-m-d H:i', strtotime('+2 hour', $streaming_end)) . '\' OR salida IS NULL) AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');
            break;

            case 'allow_newsletters':
                $columns = User::getColumnNames();
                $data = User::all([ ['acepto_publicar', 1], ['email', 'NOT REGEXP', excluded_mail_domains()] ], null);
            break;

            default:
                return null;
        endswitch;
        
        return [
            'keys' => $columns,
            'values' => $data
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function getCurrentRange()
    {
        $streaming_start = strtotime(config('streaming.date') . ' ' . config('streaming.start'));
        $streaming_end = strtotime(config('streaming.date') . ' ' . config('streaming.end'));
        if ($streaming_start > time() || $streaming_end < time()) return;

        $result = [ ];

        $now = new DateTime();
        $ranges = new DatePeriod(DateTime::createFromFormat('H:i', date('H:i', $streaming_start)), new DateInterval('PT' . config('streaming.range') . 'M'), DateTime::createFromFormat('H:i', date('H:i', $streaming_end)));
        $result = null;

        foreach ($ranges as $range):
            $from_difference = $range->diff($now);
            if ($from_difference->h > 0) continue;
            
            $to = clone $range;
            $to->modify('+' . config('streaming.range') . ' minute');

            $result = ((object) [ 'from' => $range->format('H:i'), 'to' => $to->format('H:i') ]);
        endforeach;

        return $result;
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    private function getTimeRanges()
    {
        $streaming_start = strtotime(config('streaming.date') . ' ' . config('streaming.start'));
        $streaming_end = strtotime(config('streaming.date') . ' ' . config('streaming.end'));

        $result = [ ];

        $ranges = new DatePeriod(DateTime::createFromFormat('H:i', date('H:i', $streaming_start)), new DateInterval('PT' . config('streaming.range') . 'M'), DateTime::createFromFormat('H:i', date('H:i', $streaming_end)));

        foreach ($ranges as $range):
            $to = clone $range;
            $to->modify('+' . config('streaming.range') . ' minute');

            $result[] = [
                'from' => $range->format('H:i'),
                'to' => $to->format('H:i')
            ];
        endforeach;

        return $result;
    }
}
