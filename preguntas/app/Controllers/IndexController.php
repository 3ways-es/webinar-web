<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

class IndexController extends Controller
{
    /**
     * Show the index view
     *
     * @return \App\Core\View
     */
    public function render()
    {
        return view('index');
    }
}
