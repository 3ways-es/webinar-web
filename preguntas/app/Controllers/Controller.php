<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers;

class Controller
{
    /**
     * Check permissions to show the page
     *
     * @param array $permissions
     * @param bool $api
     * 
     * @return void
     */
    public function permissions($permissions, $api = false)
    {
        $load_view = true;

        foreach ($permissions as $permission):
            switch ($permission):
                case 'auth':
                    $load_view = auth()->loggedIn();
                break;

                case 'guest':
                    $load_view = !auth()->loggedIn();
                break;
            endswitch;
        endforeach;

        if (!$load_view) die($api ? 'null' : view('error'));
    }

    /**
     * Pagination system function
     *
     * @param array $array
     * @param int $limit
     * @param int $page
     * 
     * @return array
     */
	// public function pagination($array, $limit, $page)
	// {
    //     $pages = ceil(count($array) / $limit);
    //     $result = [ ];

    //     for ($i = (($page - 1) * $limit); $i < ($page * $limit) && $i < count($array); $i++)
    //         $result['items'][] = $array[$i];

    //     $result['pages'] = $pages;
    //     $result['total'] = count($array);

    //     return $result;
	// }
}
