<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

date_default_timezone_set('Europe/Madrid');

$_SERVER['DOCUMENT_ROOT'] = __DIR__;
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/functions.php';

session_start();
initialize_app();

$_SERVER['REQUEST_URI'] = str_replace(config('app.subdir'), '', $_SERVER['REQUEST_URI']);
echo $routes->render($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
