%- @author Alexis Bogado -%
%- @package webinar-preguntas -%
%- @version 2.0.0 -%

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>%% config('app.name') %% </title>
    
    <link href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700;900&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        body {
          background-color: #6EF618;
          font-family: 'Libre Franklin';
          overflow: hidden;
          font-size: 25px;
          max-height: 100%;
          max-width: 100%;
          margin: 0
        }

        .container {
          width: 1920px !important;
          height: 1080px !important;
          text-align: center
        }

        .bottom {
        position: absolute;
        bottom: 20px;
        display: flex;
        justify-content: center;
        width: 100%
      }

      .box {
        position: relative;
        background-color: #fff;
        max-height: 272px;
        min-width: 900px;
        max-width: 1400px;
        width: auto;
        height: auto;
        box-shadow: 0 0 20px 5px rgba(0, 0, 0, 0.5);
        border-right: 10px solid #f63b60;
        text-align: left
      }

      .question {
        background-color: #f63b60;
        padding: 5px 20px;
        position: absolute;
        left: -25px;
        font-weight: 900;
        font-size: 24pt;
        color: #fff;
        box-shadow: 0 0 20px 5px rgba(0, 0, 0, 0.5);
        cursor: pointer
      }

      .text-area {
        padding: 30px 20px 30px 50px;
        word-break: break-word;
        overflow: hidden;
        max-height: 232px
      }

      #question p {
        margin: 0
      }

      #btn-audio {
        padding: 15px 30px;
        font-size: 24px;
        border-radius: 40px;
        color: white;
        background-color: red;
      }
    </style>
</head>
<body>
    <div class="container">
      <div id="recordar-audio">
        <p style="color:red">Antes de empezar es necesario pulsar éste botón</br>
          <button id="btn-audio"><i class="fa fa-play"></i></button>
          <audio src="../success.mp3" id="audio-pulsar" controls="true" style="display:none"></audio>
        </p>
      </div>

      <?php
        $respuesta = App\Controllers\QuestionsController::getCurrentScreening();
        $respuesta = json_decode($respuesta);
        $question = $respuesta[0];
        $hora_proyectar = $respuesta[1];

        $is_audio = (strpos($question, 'preguntas-audios') !== false);
      ?>

      <div class="bottom" id="question-area" <?= ($is_audio ? 'style="display:none"' : '') ?>>
        <div class="box">
          <div class="question">?</div>

          <div class="text-area">
              <span id="question">
              <?php
              if($is_audio){
                  ?> <audio src="../storage/<?php echo $question; ?>" controls="true" autoplay="true" style="display:none"></audio> <?php
              }else{
                  echo (substr($question, 0, 350) . ((strlen($question) > 350) ? '...' : '' ));
              }
              ?>
            </span>
          </div>
        </div>
      </div>
      
      <span id="hora_proyectar" style="display:none"><?=$hora_proyectar?></span>
    </div>

    <script type="text/javascript">
    audioDiv = document.getElementById("recordar-audio");
    btnAudio = document.getElementById("btn-audio");
    pulsarAudio = document.getElementById('audio-pulsar');

    btnAudio.addEventListener('click', function (){
      pulsarAudio.play();
      setTimeout(function(){
        pulsarAudio.pause();
        audioDiv.style.display = "none";
      }, 5000);
    });

    setInterval(function() {
      const http = new XMLHttpRequest();
      http.open("GET", "%% config('app.url') %%/api/questions/current_screening", true);
      http.setRequestHeader("Content-type", "application/json");
      http.onreadystatechange = function () {
        if (http.readyState != 4 || http.status != 200) return; 

        let respuesta= eval("(" + http.responseText + ")");
        let hora_nueva = respuesta[1];
        const newQuestion = respuesta[0];
        let audioQuestion = "";
        let hora_antigua = document.getElementById("hora_proyectar").innerHTML;

        if(newQuestion.includes("preguntas-audio") && newQuestion.includes(".mp3")){
          audioQuestion = '<audio src="..'+newQuestion+'" controls="true" autoplay="true" style="display:none;"></audio>'; 
          document.getElementById('question-area').style.display = "none";
        } else
          document.getElementById('question-area').style.display = "flex";

        if (hora_antigua == hora_nueva) return;
        if(newQuestion.includes("preguntas-audio") && newQuestion.includes(".mp3")){
          document.getElementById("question").innerHTML = audioQuestion;
          document.getElementById("hora_proyectar").innerHTML = hora_nueva;
        }else{
          document.getElementById("question").innerHTML = (newQuestion.substring(0, 350) + ((newQuestion.length > 350) ? '...' : ''));
        }
      };

      http.send();
    }, 3000);
    </script>
</body>
</html>