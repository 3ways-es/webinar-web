%- @author Alexis Bogado -%
%- @package webinar-preguntas -%

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>%% config('app.name') %%</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
    <link rel="stylesheet" href="%% config('app.url') %%/assets/css/app.min.css">
</head>
<body>
    <div id="app"></div>

    %- Set app configuration values in a javascript variable to use through Vue App -%
    <script>
        window.appData = {
            subdir: '%% config('app.subdir') %%',
            url: '%% config('app.url') %%',
            title: '%% config('app.name') %%',
            authByToken: %% (config('app.auth_by_token') ? 'true' : 'false') %%,
            screeningPrimaryColor: '%% config('screening.primary_color') %%',
            screeningMaxLength: %% config('screening.max_length') %%,
            releaseDate: '%% config('app.release_date') %%'
        }
    </script>
    <script src="%% config('app.url') %%/assets/js/vendors.min.js?%% time() %%"></script>
    <script src="%% config('app.url') %%/assets/js/app.min.js?%% time() %%"></script>
    <script src="%% config('app.url') %%/assets/js/icons.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>