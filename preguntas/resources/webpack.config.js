/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const jsFiles = {
    entry: {
        app: [
            'core-js/stable',
            'regenerator-runtime/runtime',
            path.resolve('./resources/js/app.js')
        ]
    },
    output: {
        path: path.resolve('./assets/js'),
        filename: '[name].min.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    modules: false,
                                    targets: {
                                        browsers: ["> 1%", "last 2 versions", "not ie <= 8", "ie >= 11"]
                                    }
                                }
                            ]
                        ],
                        plugins: [
                            "@babel/plugin-syntax-dynamic-import",
                            ["@babel/plugin-transform-arrow-functions", { "spec": true }]
                        ]
                    }
                }
            },
            {
                test: /\.vue$/,
                use: {
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            js: {
                                loader: 'babel-loader',
                                options: {
                                    presets: [
                                        [
                                            '@babel/preset-env',
                                            {
                                                modules: false,
                                                targets: {
                                                    browsers: ["> 1%", "last 2 versions", "not ie <= 8", "ie >= 11"]
                                                }
                                            }
                                        ]
                                    ],
                                    plugins: [
                                        "@babel/plugin-syntax-dynamic-import",
                                        ["@babel/plugin-transform-arrow-functions", { "spec": true }]
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        ]
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: true,
            name: 'vendors'
        }
    }
}

const sassFiles = {
    context: path.resolve('./resources/sass'),
    entry: {
        app: './app.sass'
    },
    output: {
        path: path.resolve('./assets/css')
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].min.css'
        }),
        {
            apply(compiler) {
                compiler.hooks.shouldEmit.tap('Remove js files', (compilation) => {
                    Object.keys(compilation.assets).filter(asset => asset.substring(asset.length - 3) === '.js').forEach(asset => {
                        delete compilation.assets[asset]
                    })
                
                    return true
                })
            }
        }
    ]
}

module.exports = [ jsFiles, sassFiles ]